# inputs for scheduling model with load data from Load Profile Generator

## File and folder paths. All relative to repository root (except tariffs, because it goes together with config)
## Please use FORWARD SLASH (/) to separate components in a path

# file with tariff settings, relative to THIS FILE YOU"RE READING NOW (config)
tariff file path:  "tariffs.yaml"

# specify which tariff should be used to compute ev scheduling.
# in case of capacity subscriptions, only specify "capacity subscription" here, the tariffs module will automatically
# select the best option for each household. For all other tariffs, specify the name of the particular tariff.
# use tariff: "capacity subscription"
# use tariff: "50 E/kW single personal peak"
# use tariff: "volumetric day and night tariff"
# use tariff: "Flemish tariff"
# use tariff: "250 Euro fixed charge"
use tariff: "Spanish tariff"

# cost of a flat fixed tariff to be used as benchmark
fixed tariff benchmark: 250.0

# load data
load data folder: "01-datasets/household-load/2019_Valencia"
load data file: "Data_2019_Valencia.csv"
# specify whether load data is separated into one file per household or not
# If False, it is assumed that all load data is in a single file and household loads are in columns of that file
one file per household: False

### Day-ahead prices
DA price path: "01-datasets/prices/MIBEL-DA/MIBEL_DA_price.csv"

### EV constraints
EV_constraint_path: "01-datasets/ev-constraints/remco/data/EV_Constraints_Remco.csv"

## Data format description (column names, column formats, separator, quoting)
## TODO: this should later be separated into each of the format.yaml files INSIDE the dataset folders


### Dataset-specific: time and value columns of the load data csv file
load csv time: "Time"
# if date and time columns are separate, specify date column here. Otherwise time is assumed to be datetime.
load csv date: "Date"
### date format string for python datetime format
load datetime format: "%d-%m-%Y %H:%M:%S"
# load values. Load is assumed to be supplied in kWh used from one time entry to the next.
load csv values: #relevant for one file per household only



### Dataset-specific: Prices
price csv time: "Time"
price csv values: 'Day-ahead Price [EUR/MWh]'
price datetime format: "%d-%m-%Y %H:%M"

### Dataset-specific: EV constraints
id_col: "EV_id"
arrival_time: "Start time mean"
arrival_time_std: "Start time std [h]"
departure_time: "Target time"
start_soc_mean: "Starting state of charge percentage mean"
start_soc_std: "Starting charge percentage std"
target_soc: "Target state of charge"
battery_size: "Battery size [kWh]"
max_charge: "max charge rate [kW]"
charging_efficiency: "Charging efficiency rate"
daily_demand_mean: "Daily demand mean [kWh]"
daily_demand_std: "Daily demand std [kWh]"



## TODO: all entries under here are parameters that, _combined with input data and random seed_, COMPLETELY define a run
## TODO: later move different possible configurations to the 03-configuration-examples folder, and choose a good default

## Time range and resolution

## time horizon settings

# specify over what time horizons a single optimization should run
# recommended to keep this to at most 5 days. Longer optimization takes a long time and is not realistic.
opt time horizon: "4 days"

# specify at which time intervals updates should be done, beginning at update time start
### NOTE: DA market goes from midnight to midnight
# update time start: "00:00" ### not currently used
# update time start format: "%H:%M" ### not currently used
update time interval: "1 day"

# specify whether only a single optimization run should be done, in that case also specify which start
# and end time to use. If False, the complete load curve is evaluated with rolling time optimizations,
# which are updated each day at the given update time.
single opt: False
# Optional start and end time. Both household load data and price data must contain this same range.
start datetime: "1-1-2019 12:00"
end datetime: "31-1-2019 23:00"
datetime format: "%d-%m-%Y %H:%M"

### time resolution (in minutes, has to divide 60 evenly. i.e.: 1,2,3,4,5,6,10,12,15,20,30,60)
time step: 60

graph_time_steps: 250

# Seed used for all rounds of random number generation. A number between 0 and (2^32)-1
# Two program runs with the same seed will have the EXACT SAME sequence of random numbers generated
# Passing no seed (deleting this field) means un-reproducible (pseudo random) draws
random_seed: 34985734

### Transformer maximum power in kW:
trafo capacity: 42

### household connection limit
household conn limit: 17

### Number of households (<=100):
n_hh: 21

### Number of EV"s (<=100):
n_ev: 15

### For variable EV graphs: min and max of considered EV"s:
min_ev: 1
max_ev: 10
step_ev: 3

# Number of trials to average results over:
n_trials: 3

# Whether or not to simulate with bandwidth:
bw on: True

# output of system messages during simulation run for diagnostic purposes
system messages: True
