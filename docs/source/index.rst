.. flexibleloadtariffs documentation master file, created by
   sphinx-quickstart on Thu Dec 10 14:45:51 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Modeling Flexible Load Tariffs
==============================

This is the User Guide and programming interface documentation of the _flexibleloadtariffs_ project.
It is aimed both at those who simply want to run simulations/optimizations,
as well as those interested in knowing better how the software works in order to modify/extend it.

Basic installation instructions can be found in the [README file](https://gitlab.tudelft.nl/rhenning/capacity-tariffs)
in the project's repository.

Contents
--------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   basic_usage
   simulation
   models
   households
   tariffs
   input_output


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
