What is `flexibleloadtariffs`
-----------------------------

Explain what is the point of this tool, what it is capable of doing, and what are the current limitations.


Running your first simulation
-----------------------------

Give here just one example, the simplest possible, of how to run a simulation. Everything default, etc.


The folder structure
--------------------

* Explain what is stored in each of the folders in the repo
* Give special attention to the difference between
  + `configurations` and `configuration-examples`
  + `outputs` and `output-examples`


Command line parameters and options
-----------------------------------

Here explain:

* The obligatory positional parameters
  + As far as I know, this is only the _strategy_ now
* The options
  + In both long and short form (single letter option and "normal" name)
    - `--config`
    - `--varevs`
    - `-h` (help about options/parameters)
