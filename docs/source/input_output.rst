Datasets
--------

Explain the general format and origin of the different datasets used as input for modeling:

* Prices
* Load curves
* EV constraints


Parameters
----------

Go over the most important parameters for simulation (found in the configuration files)


Output
------

* What exactly is the generated output for each type of simulation (different strategies etc.)
  + Where each piece of output is stored at
* How to interpret the outputted tables / graphs