Simulation module
-----------------

The entry point where input data and parameters are read, the optimization is run, and the outputs are generated.