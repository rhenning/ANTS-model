from datetime import datetime
from pathlib import Path
from functools import reduce
import numpy as np
import pandas as pd
from copy import deepcopy
import matplotlib.pyplot as plt

from flexibleloadtariffs.utils.load_inputs import load_yaml_config, load_single_time_series_from_csv
from flexibleloadtariffs.utils.random_draws import initialize_with_seed

reporoot_dir = Path(__file__).resolve().parent.parent

config = {}


def simulate(args):
    config_file_absolute = Path(args.config).resolve()
    config_dir_absolute = config_file_absolute.parent

    globals()['config'] = load_yaml_config(config_file_absolute)

    from flexibleloadtariffs.households import load_households, compute_best_bw_tariff
    from flexibleloadtariffs.neighborhood import Neighborhood
    from flexibleloadtariffs.tariffs import instantiate_tariffs
    from flexibleloadtariffs.evs import load_ev_constraints, variable_ev_runs
    from flexibleloadtariffs.performance import network_diagnostics, total_tariff_cost_per_kWh, total_tariff_charge
    from flexibleloadtariffs.utils.plots import load_bar_graph
    from flexibleloadtariffs.utils.write_outputs import ev_power_to_csv, variable_ev_json_output, total_power_to_csv, \
        household_results_csv, network_powercurve_csv, network_summary_csv
    from flexibleloadtariffs.network_summary import compute_cost_correlations, compute_nonfixed_cost_contributions, \
        network_peak, load_factor, ev_charging_cost_daprice, compute_contributing_peak_times

    tariff_inputs = load_yaml_config(config_dir_absolute / config['tariff file path'])
    tariffs = instantiate_tariffs(tariff_inputs)


    # set whether to use bandwidth constraint in capacity subscriptions
    bw_on = config['bw on']

    # total_power_np_array = np.array(total_hh_power_ts.values.tolist()).flatten()
    # power_to_csv(total_hh_power_ts.resample('{}T'.format('60')).mean())

    # make time selection
    datetime_format = config['datetime format']
    opt_time_horizon = pd.to_timedelta(config['opt time horizon'])
    update_time_interval = pd.to_timedelta(config['update time interval'])
    start_datetime = datetime.strptime(config['start datetime'], datetime_format)
    end_datetime = datetime.strptime(config['end datetime'], datetime_format)
    time_step = config['time step']
    system_messages = config['system messages']

    # load Day Ahead Price curve, convert to euro per kWh from euro per MWh
    da_price_ts = (load_single_time_series_from_csv(config['DA price path'], config['price csv time'],
                                        config['price csv values'], config['price datetime format'])).mul(0.001)
    # resample to given time step intervals, padding the result at the intermediate time steps
    da_price_ts = da_price_ts.resample('{}T'.format(time_step)).pad()

    # load household data and create household objects
    households = load_households(config)
    # compute total power curve
    # household_powers = (hh.inflex_power for hh in households.values())
    # total_hh_power_ts = reduce(lambda x, y: x.add(y), household_powers)
    tariff_rev_req = config['fixed cost'] * len(households)

    def single_scheduling_run(households, config, trafo_size):
        # initialize neighborhood
        neighborhood = Neighborhood(households, da_price_ts, trafo_size,
                                    config['trafo loss factor'], config['trafo upgrade factor'],
                                    config['trafo replacement fraction'],
                                    args.strategy, tariff_rev_req, config['fixed cost']*config['n_hh'],
                                    start_datetime)
        if args.strategy != 'no-ev':
            # run simulation of neighborhood until end date is reached
            while neighborhood.current_date < end_datetime - opt_time_horizon:
                neighborhood.update_simulation(update_time_interval, opt_time_horizon)
        return neighborhood

    indicator_key_list = {"Total non-fixed cost",
                          "Cost correlation EV", "Cost correlation no EV", "Cost correlation all",
                          "Network peak size", "Network peak EVs", "Network peak households",
                          "Load factor",
                          "Total EV charging cost"}

    for test_tariff in config['use tariffs']:
        print("Computing {} results".format(test_tariff))
        new_tariff_households = deepcopy(households)
        for hh in new_tariff_households.values():
            if test_tariff == 'capacity subscription':
                hh.tariff = compute_best_bw_tariff(hh.total_power, tariffs)
            else:
                hh.tariff = tariffs[test_tariff]
            hh.capacity_limit_by_month = hh.compute_capacity_limit_by_month()

        if not args.varevs:
            single_scheduling_run(households, test_tariff, config)

        else:
            ev_numbers = range(config['min_ev'], config['max_ev'] + 1, config['step_ev'])
            indicator_dict = {}
            for indicator in indicator_key_list:
                indicator_dict[indicator] = {n_ev:
                                                 {mean_std_20_80perc: 0 for mean_std_20_80perc in
                                                  ["mean", "std", "20 perc", "80 perc"]}
                                             for n_ev in ev_numbers }
            for n_ev in ev_numbers:
                # re-set the random seed to make results consistent across tariffs and different EV numbers
                initialize_with_seed(config.get('random_seed', None))
                if system_messages:
                    print("n_ev = " + str(n_ev))
                # note: it's a bit weird that here an input parameter in the config file is overwritten and then
                # passed to the single scheduling run function in the config file
                config['n_ev'] = n_ev
                trafo_sizes = range(config['trafo size min'], config['trafo size max'], config['trafo size step'])
                indicators_temp = {indicator: np.zeros(len(trafo_sizes) * config['n trials'])
                                   for indicator in indicator_key_list}
                i = 0
                for trafo_size in trafo_sizes:
                    if system_messages:
                        print("trafo size = " + str(trafo_size))
                    for trial in range(config['n trials']):
                        if system_messages:
                            print("trial " + str(trial+1))
                        # create copy to flush previous writing of ev load on households
                        temp_households = deepcopy(new_tariff_households)
                        # load dictionary of EV constraints and charging objectives
                        load_ev_constraints(config, temp_households)
                        neighborhood = single_scheduling_run(temp_households, config, trafo_size)
                        contributing_peak_times = compute_contributing_peak_times(neighborhood)
                        cost_correlations = compute_cost_correlations(neighborhood, contributing_peak_times)
                        nonfixed_cost_contributions = compute_nonfixed_cost_contributions(neighborhood,
                                                                                          contributing_peak_times)
                        indicators_temp["Cost correlation EV"][i] = cost_correlations["With EV"]
                        indicators_temp["Cost correlation no EV"][i] = cost_correlations["No EV"]
                        indicators_temp["Cost correlation all"][i] = cost_correlations["All"]
                        indicators_temp["Network peak size"][i], indicators_temp["Network peak EVs"][i], \
                        indicators_temp["Network peak households"][i] = network_peak(neighborhood, trafo_size)
                        indicators_temp["Load factor"][i] = load_factor(neighborhood)
                        x, indicators_temp["Total EV charging cost"][i] = ev_charging_cost_daprice(neighborhood)
                        indicators_temp["Total non-fixed cost"][i] = nonfixed_cost_contributions
                        # write trial-specific outputs only once per parameter combination
                        if trial == 0:
                            if "households summary" in config['write outputs']:
                                household_results_csv(neighborhood, test_tariff, config['n_hh'], config['n_ev'], trafo_size,
                                                      trial, contributing_peak_times)
                            if "households EV load" in config['write outputs']:
                                ev_power_to_csv(neighborhood.households, test_tariff, config['n_hh'], config['n_ev'], trafo_size, trial)
                            if "households total load" in config['write outputs']:
                                total_power_to_csv(neighborhood.households, test_tariff, config['n_hh'], config['n_ev'], trafo_size,
                                                   trial)
                            if "network load" in config['write outputs']:
                                network_powercurve_csv(neighborhood, test_tariff, config['n_hh'], config['n_ev'],
                                                       trafo_size, trial)
                        i += 1
                for indicator in indicator_key_list:
                    print("{}: {}".format(indicator, indicators_temp[indicator]))
                    indicator_dict[indicator][n_ev]["mean"] = np.mean(indicators_temp[indicator])
                    indicator_dict[indicator][n_ev]["std"] = np.std(indicators_temp[indicator])
                    indicator_dict[indicator][n_ev]["20 perc"] = np.percentile(indicators_temp[indicator], 20)
                    indicator_dict[indicator][n_ev]["80 perc"] = np.percentile(indicators_temp[indicator], 80)
            if "network summary" in config['write outputs']:
                network_summary_csv(indicator_dict, test_tariff, config['n_hh'])

    # plots and outputs, happens regardless of strategy chosen
    # graph_time_steps = len(total_ev_power_np_array)
    # graph_first_time_step = 24*int(60/time_step)
    # graph_last_time_step = 640*int(60/time_step)
    # load_bar_graph(total_power_np_array[graph_first_time_step:graph_last_time_step],
    #                total_ev_power_np_array[graph_first_time_step:graph_last_time_step],
    #                np.arange(graph_last_time_step-graph_first_time_step),
    #                config['trafo capacity'], n_ev=config['n_ev'])

    ### power-duration curve
    # power = np.sort(total_power_np_array + total_ev_power_np_array)[::-1]
    # duration = np.arange(power.size)
    #
    # fig, ax = plt.subplots()
    # ax.plot(duration, power, config['trafo capacity'])
    #
    # ax.set(xlabel='time', ylabel='power (kW)',
    #        title='power-Duration curve for ' + config['use tariff'])
    # ax.grid()
    #
    # plt.show()



    ### tariff charges relative to cost contributions
    # x_data = [cost_contr_total(neighborhood)[i] for i in range(50)]
    # y_data = [total_tariff_charge(households)[i] for i in range(50)]
    #
    # fig, ax = plt.subplots()
    # ax.scatter(x_data, y_data)
    #
    # ax.set_xlabel('Household Cost Contribution', fontsize=15)
    # ax.set_ylabel('Household tariff charge', fontsize=15)
    # ax.set_title('Tariff charges relative to cost contributions')
    #
    # ax.grid(True)
    # fig.tight_layout()

    # plt.show()



    # variable EV number runs
    # if args.varevs:
    #     variable_ev_runs_max_power = variable_ev_runs(households, config, da_price_ts, total_power_np_array)
    #     variable_ev_json_output(variable_ev_runs_max_power)
    #
    # return
