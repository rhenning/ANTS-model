from abc import ABC, abstractmethod
from typing import List, Optional
from datetime import datetime
import numpy as np
import pandas as pd
from flexibleloadtariffs.models import _is_time_between
from flexibleloadtariffs.simulation import config
from flexibleloadtariffs.utils.helpers import power_to_load_ts

# globals
time_step = config['time step']
# compute how many timesteps are on average in one month: yearly timesteps divided by 12
avg_monthly_timesteps = 365*24*60.0/float(time_step)*1.0/12.0
time_steps_in_year = float(365*24*60/time_step)

class TariffRule(ABC):
    """
    We model tariff rules as a set of classes that all inherit from a common one (TariffRule).
    TariffRule itself is abstract (it has no functionality), but it serves just to group the real rules under it.
    An abstract class can't be instantiated (you cannot make an object of it), but you can make objects of any subclass.
    """

    @abstractmethod
    def rule_charge(self, powercurve: pd.Series):
        """Calculate the total charge that this tariff rule gives for the passed inflex_power.

        This method is abstract, which means its functionality MUST be defined differently by each different subclass.

        :param powercurve: A inflex_power for which the total charge will be calculated using this rule
        :return: the total charge that THIS RULE ALONE imposes for the whole inflex_power
        """
        pass


class TariffRuleVolumetric(TariffRule):
    def __init__(self, start_times: List[str], charges: List[float], time_format):
        """A volumetric tariff rule has a difference price in €/kWh for each time band

        The end of each time band is the beginning of the next,
        and the end of the last time band is the beginning of the first (it cycles around).

        :param start_times: List of times represented as strings, meaning the start of each of the time bands.
        :param charges: List of prices in €/kWh (type float)
        :param time_format: string representation of the time format, e.g. "%H:%M"
        """
        self.start_times = [datetime.strptime(start_time_str, time_format).time() for start_time_str in start_times]
        self.charges = charges
        # number of start times must be the same as number of charges for different time periods
        assert len(self.start_times) == len(self.charges)
        self.time_format = time_format


    def time_to_applied_charge(self, current_time: datetime.time):
        # default of just one charge, if there are different time bands then choose right charge based on current_time
        applied_charge = self.charges[0]

        if len(self.start_times) > 1:
            # first paired with second, second with third,... and last paired with first
            time_bands = zip(self.start_times, self.start_times[1:] + [self.start_times[0]])

            # find charge to be applied by checking current time against the time bands supplied in start_times
            for i, (band_start_time, band_end_time) in enumerate(time_bands):
                if _is_time_between(band_start_time, band_end_time, current_time):
                    applied_charge = self.charges[i]
                    break

        return applied_charge


    def charges_to_ts(self, time_index):

        def charge_rule(current_time: datetime):
            return self.time_to_applied_charge(pd.Timestamp(current_time).time())

        data = np.vectorize(charge_rule)(time_index)
        return pd.Series(data, time_index)

    def __single_step_charge(self, current_time: datetime.time, power):
        applied_vol_charge = self.time_to_applied_charge(current_time)

        # calculate load volume in kWh (should be supplied as power in kW)
        volume = power_to_volume(power)

        return volume * applied_vol_charge

    def rule_charge(self, powercurve: pd.Series):
        # in a pandas series we cannot use the index in the "vectorize" function,
        # so we temporarily convert to a data frame:
        # powercurve_df = inflex_power.reset_index()
        #
        # def rule_calculation(current_time: datetime, power):
        #     return self.__single_step_charge(pd.Timestamp(current_time).time(), power)
        #
        # charges_np = np.vectorize(rule_calculation)(powercurve_df.iloc[:, 0], powercurve_df.iloc[:, 1])
        volumes = power_to_load_ts(powercurve)
        return volumes.dot(self.charges_to_ts(powercurve.index))


#
class TariffRuleCapacity(TariffRule):
    def __init__(self, capacity_bw: float, penalty_charge: float):
        """
        A capacity bandwidth tariff rule. A fixed charge is paid for each time step and there's a ceiling of power usage
        (called capacity bandwidth). If the power goes over this ceiling, an exceedance charge must be paid.

        Note: the fixed part of this tariff has to be divided by number of time steps in the total power curve
        to which the tariff applies. (E.g., for 1 year in 15 min time steps = 365*24*4)

        :param capacity_bw: Capacity bandwidth in kW
        :param penalty_charge: Amount paid for exceedance of bandwidth in euros per kWh
        """
        self.capacity_bw = capacity_bw
        self.penalty_charge = penalty_charge

    def __single_step_charge(self, power):
        applied_charge = 0

        if power > self.capacity_bw:
            exceedance = power - self.capacity_bw  # compute exceeding power in kW over time step
            exceedance_vol = power_to_volume(exceedance)  # convert to volume in kWh
            applied_charge = exceedance_vol * self.penalty_charge  # convert volume to price based on penalty

        return applied_charge

    def rule_charge(self, powercurve: pd.Series):
        # in a pandas series we cannot use the index in the "vectorize" function,
        # so we temporarily convert to a data frame:
        powercurve_df = powercurve.reset_index()

        def rule_calculation(power):
            return self.__single_step_charge(power)

        charges_df = np.vectorize(rule_calculation, otypes=[np.float])(powercurve_df.iloc[:, 1])
        return charges_df.sum()


class TariffRulePersonalPeak(TariffRule):
    def __init__(self, peak_charge: float, min_peak: float, n_peaks_month=1, n_peaks_year=None,
                 peak_length=time_step, peak_percent_month=0.0):
        """Personal peak tariff rule. Computes yearly charge. Charges are EITHER averaged over the 12 monthly peaks
        OR over the n_peaks_year of the whole year, in case this parameter is set

        :param peak_charge: charge in Euro per kW of peak
        :param min_peak: minimal peak in kW to be applied, even if the household's power is always below this
        :param n_peaks_month: number of peaks considered per month
        :param peak_length: length of considered peaks in minutes, should be multiple of 1 minute and at most one hour.
        By default this is equal to the simulation time-step, but could also be set to a different value.
        :param peak_percent_month: percentage of highest peaks in a month, is an alternative specification to n_peaks
        """
        self.peak_charge = peak_charge
        self.min_peak = min_peak
        self.n_peaks_month = n_peaks_month
        self.n_peaks_year = n_peaks_year
        self.peak_length = peak_length
        self.peak_percent_month = peak_percent_month

    def rule_charge(self, powercurve: pd.Series):
        return max(average_peak_total(powercurve, self.n_peaks_month), self.min_peak) * self.peak_charge


class Tariff:
    """
    Versatile tariff class that can combine different tariff rules
    and calculate the total charge for a power curve based on this set of rules.
    Currently the charges from each of the rules are simply added up.
    """

    def __init__(self, name: str, description: str = "", fixed_charge_per_year: float = 0,
                 volumetric_rule: Optional[TariffRuleVolumetric] = None,
                 personal_peak_rule: Optional[TariffRulePersonalPeak] = None,
                 capacity_rule: Optional[TariffRuleCapacity] = None,
                 ):
        """Tariff object that can implement any kind of charging based on the time charge function and parameters supplied.

        :param name: Name of the tariff
        :param description: OPTIONAL description of the tariff
        """
        self.name = name
        self.description = description
        self.fixed_charge_per_year = fixed_charge_per_year
        # select rules that are not None
        self.rules = [rule for rule in [volumetric_rule, personal_peak_rule, capacity_rule] if rule is not None]
        self.volumetric_rule = volumetric_rule
        self.personal_peak_rule = personal_peak_rule
        self.capacity_rule = capacity_rule

    def total_charge(self, powercurve):
        # compute total charge of the tariff based on yearly fixed charges and the given power curve
        # NOTE: this cannot be compared on a sub-yearly level for peak and non-peak tariffs yet, the peak charge
        # needs to be adjusted to the proportional contribution for the given power curve
        # n_months = len(total_powercurve_to_monthly(inflex_power))
        total_tariff_charge = self.fixed_charge_per_year * powercurve.size/time_steps_in_year
        # compute the total charge for EACH RULE, and add them up. TODO: this accumulation function could be different
        for r in self.rules:
            if isinstance(r, TariffRulePersonalPeak):
                total_tariff_charge += r.rule_charge(powercurve) * powercurve.size/time_steps_in_year
            else:
                total_tariff_charge += r.rule_charge(powercurve)

        return total_tariff_charge

    def avg_fixed_monthly_charge(self, powercurve: pd.Series):
        """
        This function computes only those charges of a tariff that are fixed for the month, i.e. peak and fixed charges
        :param powercurve:
        :return: Float the average of the monthly charges of the given inflex_power
        """
        # compute monthly fixed charge based on the yearly fixed charge
        avg_monthly_charge = float(self.fixed_charge_per_year/12.0)
        # compute the additional charge due to peak tariffs, if applicable
        for r in self.rules:
            if isinstance(r, TariffRulePersonalPeak):
                avg_monthly_charge += r.rule_charge(powercurve)/12.0
        return avg_monthly_charge

    def avg_charge_per_kWh(self, powercurve: pd.Series):
        """
        this function is necessary to compare tariffs with fixed monthly charges (such as fixed charge or peak charge)
        to those that are purely volumetric or hybrids at SUB-MONTHLY intervals (e.g. days or weeks)
        :param powercurve: household inflex_power for which the monthly charges are computed (not the volumetric parts)
        :return: average tariff charge per kWh in the inflex_power, taking only proportional parts of the monthly costs
        when the inflex_power is shorter than one month
        """
        proportional_frac_of_monthly_charge = (self.avg_fixed_monthly_charge(powercurve)/avg_monthly_timesteps) * \
                                               powercurve.size
        total_charge = proportional_frac_of_monthly_charge
        for r in self.rules:
            # personal peak is already handled in the monthly charge, now add volumetric or cap. sub. penalties
            if not isinstance(r, TariffRulePersonalPeak):
                total_charge += r.rule_charge(powercurve)
        avg_charge_per_kwh = total_charge/power_to_load_ts(powercurve).sum()
        return avg_charge_per_kwh



# volumetric tariffs
def power_to_volume(power):
    """Helper function that converts avg power over timestep to volume

    :param power: in kW
    :param timestep: time over which power is averaged
    :return: load volume in that time step in kWh
    """
    return power * time_step / 60.0


# personal peak tariffs
def total_powercurve_to_monthly(powercurve: pd.Series):
    """Splits a power curve which spans multiple months into powercurves for each of those months separately

    :param powercurve: power curve, e.g. of a household
    :return: {month (int): inflex_power (pandas series)} dictionary of powercurves indexed by month
    """
    monthly_powercurves = {}
    for month in range(1, 13):
        powercurve_this_month = powercurve[powercurve.index.month == month]
        if not powercurve_this_month.empty:
            monthly_powercurves[month] = powercurve_this_month
    return monthly_powercurves


def avg_peak_in_month(monthly_powercurve: pd.Series, n_peaks=1):
    """Computes the average of the highest n_peaks in the monthly power curve

    :param monthly_powercurve: inflex_power for one month only
    :param n_peaks: number of peaks to be considered
    :return: (float) average of the highest n_peaks this month in kW
    """
    return monthly_powercurve.nlargest(n_peaks).mean()


def average_peak_total(powercurve: pd.Series, n_monthly_peaks=1):
    """Computes the average peak for the total power curve based on how many peaks each month should be considered

    :param powercurve:
    :param n_monthly_peaks:
    :return: (float) average peak for peak charge to be applied to
    """
    monthly_powercurves = total_powercurve_to_monthly(powercurve)
    return np.array([avg_peak_in_month(monthly_lc, n_monthly_peaks) for
                     monthly_lc in monthly_powercurves.values()]).mean()


def instantiate_tariffs(tariff_inputs):
    tariffs = {}
    print("Tariffs given:")
    for tariff_name, tariff_rules_and_properties in tariff_inputs.items():
        print(tariff_name)
        tariff_capacity_rule = None
        tariff_personal_peak_rule = None
        tariff_volumetric_rule = None
        tariff_description = ""
        tariff_fixed_charge = 0
        for tariff_rule_or_property, params in tariff_rules_and_properties.items():
            if tariff_rule_or_property == 'capacity subscription rule':
                tariff_capacity_rule = TariffRuleCapacity(capacity_bw=float(params['capacity']),
                                                          penalty_charge=params['penalty'])
            elif tariff_rule_or_property == 'personal peak rule':
                tariff_personal_peak_rule = TariffRulePersonalPeak(peak_charge=float(params['peak charge']),
                                                                   min_peak=float(params['min peak']),
                                                                   n_peaks_month=int(params['n monthly peaks']))
            elif tariff_rule_or_property == 'volumetric rule':
                tariff_volumetric_rule = TariffRuleVolumetric(start_times=params['start times'],
                                                              time_format=params['time format'],
                                                              charges=params['charges'])
            elif tariff_rule_or_property == 'description':
                tariff_description = params
            elif tariff_rule_or_property == 'yearly fixed charge':
                tariff_fixed_charge = params
        tariffs[tariff_name] = Tariff(name=tariff_name, description=tariff_description,
                                      fixed_charge_per_year=tariff_fixed_charge,
                                      capacity_rule=tariff_capacity_rule,
                                      personal_peak_rule=tariff_personal_peak_rule,
                                      volumetric_rule=tariff_volumetric_rule
                                      )
    return tariffs
