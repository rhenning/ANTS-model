from yaml import load, FullLoader
import pandas as pd
from pathlib import Path

reporoot_dir = Path(__file__).resolve().parent.parent.parent


def load_yaml_config(file_path):
    with open(file_path) as f:
        configs = load(f, Loader=FullLoader)
    return configs


def load_single_time_series_from_csv(csv_filepath, time_col, value_col, datetime_format, decimal_separator='.'):
    df = pd.read_csv(csv_filepath, sep=';', decimal=decimal_separator, header=0, index_col=False)
    df[time_col] = pd.to_datetime(df[time_col], format=datetime_format)
    # convert data frame to time series
    ts = pd.Series(data=pd.to_numeric(df[value_col].values), index=df[time_col])
    return ts
