from datetime import datetime
from pathlib import Path
import pandas as pd
from functools import reduce
from json import dump

from flexibleloadtariffs.utils.helpers import power_to_load_ts
from flexibleloadtariffs.network_summary import cost_contr_total, cost_contr_loss, cost_contr_fixed, \
    cost_contr_replacement, ev_charging_cost_daprice


reporoot_dir = Path(__file__).resolve().parent.parent.parent
outputs_dir = reporoot_dir / "10-outputs"

timestamp_format = "{:%Y%m%dT%H%M}"


def create_filename(prefix="", args=None):
    # name is combination of current date_time and input arguments key/value pairs
    timestamp = timestamp_format.format(datetime.now())
    if args:
        # may have slashes (either back or forward) because of filenames in cfgval, so we filter them to avoid confusion
        inputargs_raw = '_'.join([f"{cfgval}" for cfgkey, cfgval in args.items() if cfgval])
        inputargs_filtered = inputargs_raw.replace("/", "_").replace("\\", "_").replace(
            "03-configuration-examples_", "").replace(".yaml", "").replace(" ", "_")
        filename = f"{prefix}__{inputargs_filtered}.csv"
    else:
        filename = f"{prefix}__{timestamp}.csv"
    return filename

def ev_power_to_csv(households, tariff, n_hh, n_ev, trafo_size, trial, filename_prefix = "ev_power"):
    output_df = pd.concat([households[ident].ev_power for ident in sorted(households.keys())], axis=1)
    inputargs = {"tariff": tariff, "n_hh": str(n_hh) + "hh", "n_ev": str(n_ev) + "evs",
                 "Trafo": "Trafo" + str(trafo_size) + "kW", "Trial": "Trial" + str(trial)}
    filename = create_filename(filename_prefix, args=inputargs)
    # create if doesn't exist yet and open for writing (mode 'w+'), use no newlines (pandas requires this)
    output_filepath = outputs_dir / filename
    with open(output_filepath, newline='', mode='w+') as file:
        output_df.to_csv(file, sep=';')

def total_power_to_csv(households, tariff, n_hh, n_ev, trafo_size, trial, filename_prefix = "total_power"):
    output_df = pd.concat([households[ident].total_power for ident in sorted(households.keys())], axis=1)
    inputargs = {"tariff": tariff, "n_hh": str(n_hh) + "hh", "n_ev": str(n_ev) + "evs",
                 "Trafo": "Trafo" + str(trafo_size) + "kW", "Trial": "Trial" + str(trial)}
    filename = create_filename(filename_prefix, args=inputargs)
    # create if doesn't exist yet and open for writing (mode 'w+'), use no newlines (pandas requires this)
    output_filepath = outputs_dir / filename
    with open(output_filepath, newline='', mode='w+') as file:
        output_df.to_csv(file, sep=';')


def household_results_csv(neighborhood, tariff, n_hh, n_ev, trafo_size, trial, contributing_peak_times,
                          filename_prefix = "households"):
    inputargs = {"tariff": tariff, "n_hh": str(n_hh) + "hh", "n_ev": str(n_ev) + "evs",
                 "Trafo": "Trafo" + str(trafo_size) + "kW", "Trial": "Trial" + str(trial)}
    filename = create_filename(filename_prefix, args=inputargs)
    output_filepath = outputs_dir / "results" / filename
    outputs = ['total consumption kWh', 'total tariff charge', 'tariff charge per kWh', 'tariff charge inflex',
               'extra tariff charge for EV', 'total EV consumption kWh',
               'total cost contribution', 'contribution to losses', 'contribution to fixed cost',
               'contribution to early repl.', 'maximal power kW', 'average power kW',
               'EV charging costs day-ahead price']
    output_df = pd.DataFrame(index=outputs)
    loss_cost_dict = cost_contr_loss(neighborhood)
    fixed_cost_dict = cost_contr_fixed(neighborhood)
    repl_cost_dict = cost_contr_replacement(neighborhood, contributing_peak_times)
    charge_cost_dict, x = ev_charging_cost_daprice(neighborhood)
    for hh_id, hh in neighborhood.households.items():
        total_cons = power_to_load_ts(hh.total_power).sum()
        tariff_charge = hh.tariff.total_charge(hh.total_power)
        tariff_charge_inflex = hh.tariff.total_charge(hh.inflex_power)
        tariff_charge_EV = tariff_charge - tariff_charge_inflex
        charge_kwh = tariff_charge/float(total_cons)
        total_ev = power_to_load_ts(hh.ev_power).sum()
        total_cost_contr = repl_cost_dict[hh.ident] + fixed_cost_dict[hh.ident] + loss_cost_dict[hh.ident]
        loss_cost_contr = loss_cost_dict[hh.ident]
        fixed_cost_contr = fixed_cost_dict[hh_id]
        repl_cost_contr = repl_cost_dict[hh_id]
        max_power = hh.total_power.max()
        avg_power = hh.total_power.mean()
        ev_charging_costs = charge_cost_dict[hh_id]
        output_df[str(hh_id)] = [total_cons, tariff_charge, charge_kwh, tariff_charge_inflex, tariff_charge_EV,
                                 total_ev, total_cost_contr, loss_cost_contr,
                                 fixed_cost_contr, repl_cost_contr, max_power, avg_power, ev_charging_costs]
    with open(output_filepath, newline='', mode='w+') as file:
        output_df.to_csv(file, sep=';')


def network_powercurve_csv(neighborhood, tariff, n_hh, n_ev, trafo_size, trial, filename_prefix = "network_load"):
    inputargs = {"tariff": tariff, "n_hh": str(n_hh) + "hh", "n_ev": str(n_ev) + "evs",
                 "Trafo": "Trafo" + str(trafo_size) + "kW", "Trial": "Trial" + str(trial)}
    filename = create_filename(filename_prefix, args=inputargs)
    output_filepath = outputs_dir / "results" / filename
    data = {'total_power': neighborhood.total_power_ts, 'total_inflexible': neighborhood.total_inflex_power_ts,
            'total_ev': neighborhood.total_ev_power_ts}
    network_df = pd.DataFrame(data, index=neighborhood.total_inflex_power_ts.index)
    with open(output_filepath, newline='', mode='w+') as file:
        network_df.to_csv(file, sep=';')


def network_summary_csv(network_indicators_dict, tariff, n_hh, filename_prefix = "network_summary"):
    inputargs = {"tariff": tariff, "n_hh": str(n_hh) + "hh"}
    filename = create_filename(filename_prefix, args=inputargs)
    output_filepath = outputs_dir / "results" / filename
    indicator_list = sorted([indicator for indicator in network_indicators_dict.keys()])
    outputs = [indicator + mean_std_20_80perc
               for indicator in indicator_list
               for mean_std_20_80perc in [" mean", " std", " 20 perc", " 80 perc"]
               ]
    output_df = pd.DataFrame(index=outputs)
    ev_numbers = [n_ev for n_ev in next(iter(network_indicators_dict.values())).keys()]
    for n_ev in ev_numbers:
        output_df[str(n_ev)] = [network_indicators_dict[indicator][n_ev][mean_std_20_80perc]
                                for indicator in indicator_list
                                for mean_std_20_80perc in ["mean", "std", "20 perc", "80 perc"]
                                ]
    with open(output_filepath, newline='', mode='w+') as file:
        output_df.to_csv(file, sep=';')


def variable_ev_json_output(max_power_dict):
    for charge_strategy in max_power_dict.keys():
        max_power_dict[charge_strategy] = max_power_dict[charge_strategy].tolist()

    timestamp = timestamp_format.format(datetime.now())
    filename = f"{timestamp}_transformer_loading.json"

    # create if doesn't exist yet and open for writing (mode 'w+')
    output_filepath = outputs_dir / "transformer_loading" / filename
    with open(output_filepath, 'w+', encoding='utf-8') as f:
        dump(max_power_dict, f, ensure_ascii=False, indent=4)
