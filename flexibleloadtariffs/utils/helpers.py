import pandas as pd
from flexibleloadtariffs.simulation import config

# globals
time_step = config['time step']


def load_to_power_ts(time_series: pd.Series):
    return time_series.multiply(60 / time_step)


def power_to_load_ts(time_series: pd.Series):
    return time_series.multiply(time_step / 60)


def load_to_power_value(value: float):
    return value * 60 / time_step


def power_to_load_value(value: float):
    return value * time_step / 60
