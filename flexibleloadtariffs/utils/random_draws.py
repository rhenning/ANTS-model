from numpy import random
from math import ceil
import pandas as pd


# must be called before drawing any random data (a good place is at the start of the simulation.py simulate method)
def initialize_with_seed(random_seed: str = None):
    random.seed(random_seed)


# random time generation
def draw_time(mean, std, time_step):
    """
    computes a random time based on given mean time and std rounded up to nearest time step
    :param mean: mean time as timestamp
    :param std: std in hours
    :param time_step: time step to round result to
    :return: timestamp object with randomly drawn time
    """

    def myround(x, base):
        # rounds up to the nearest time step
        return base * ceil(x / base)

    mean_time_float = float(mean.hour) + float(mean.minute) / 60
    draw_time_float = random.normal(mean_time_float, std)
    hour = int(draw_time_float)
    minute = int((draw_time_float % 1) * 60)
    minute_time_step = myround(minute, time_step)
    if minute_time_step == 60:
        hour += 1
        minute_time_step = 0
    hour = hour % 24
    time = pd.Timestamp(year=2020, month=1, day=1, hour=hour, minute=minute_time_step)
    return time


# random state of charge from mean and standard deviation
def draw_soc(mean, std):
    soc = random.normal(mean, std)
    # redraw if state of charge is smaller than 0 or bigger than 1
    while soc < 0 or 1 < soc:
        soc = random.normal(mean, std)
    return soc


# random daily demand from mean and standard deviation
def draw_daily_demand(mean, std, target_charge):
    daily_demand = random.normal(mean, std)
    # redraw if demand is below 0 or bigger than minimum target charge
    while daily_demand < 0 or target_charge < daily_demand:
        daily_demand = random.normal(mean, std)
    return daily_demand

