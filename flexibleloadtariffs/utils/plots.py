import matplotlib.pyplot as plt
import numpy as np


def variable_ev_transformer_power_plot(variable_ev_runs_max_power, ev_index):
    # create a color palette
    palette = plt.get_cmap('Set1')
    num = 0
    for charge_strategy in variable_ev_runs_max_power.keys():
        plt.plot(ev_index, variable_ev_runs_max_power[charge_strategy], color=palette(num), label=charge_strategy)
        num += 1
    # Add legend
    plt.legend(loc=2, ncol=1)
    plt.xlabel("Number of EV's")
    plt.ylabel("Maximal Transformer Loading in %")
    axes = plt.gca()
    # make y axis selection in percentage relative to transformer capacity
    axes.set_ylim([25, 125])
    plt.show()


# TODO: figure out how/what to use as bars here (p1,p2,p3, etc.)
# TODO: also remove or decide how to use dead code (comented out)
# noinspection PyUnusedLocal
# bar graph plotting base power and ev power on top of each other
def load_bar_graph(base_power, ev_power, time_ind, transformer_limit, n_ev):
    fig, ax = plt.subplots()

    p1 = plt.bar(time_ind, base_power, width=0.7)
    p2 = plt.bar(time_ind, ev_power, bottom=base_power, color='orange', width=0.7)
    p3 = plt.plot(time_ind, np.full(len(time_ind), transformer_limit), ls='--', color='red')

    plt.xlabel('Time Step')
    plt.ylabel('power at Transformer in kW')
    # plt.xticks([i for i in time_ind][::96])
    # plt.xticks(total_power_index[::96])
    # plt.legend((p1[0], p2[0], p3[0]), ('Non-EV power', 'EV-power', 'Rated Transformer Capacity'), loc=1)
    plt.text(0.05, 0.95, str(n_ev) + ' EVs', fontsize=14, transform=ax.transAxes, verticalalignment='top')
    plt.show()


# scatter plot plotting charge per kWh vs household BW with scatter plots for both
def scatter_hist(x, y):
    # definitions for the axes
    left, width = 0.1, 0.65
    bottom, height = 0.1, 0.65
    spacing = 0.005

    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom + height + spacing + 0.005, width, 0.2]
    rect_histy = [left + width + spacing, bottom, 0.2, height]

    # start with a square Figure
    fig = plt.figure(figsize=(8, 8))

    ax = fig.add_axes(rect_scatter)
    ax_histx = fig.add_axes(rect_histx, sharex=ax)
    ax_histy = fig.add_axes(rect_histy, sharey=ax)
    # labels
    ax_histx.tick_params(axis="x", labelbottom='Bandwidth [kW]')
    ax_histy.tick_params(axis="y", labelleft='Charge per kWh [Euro]')

    # the scatter plot:
    ax.scatter(x, y)
    ax.set_xlabel('Bandwidth [kW]')
    ax.set_ylabel('Charge per kWh [Euro]')

    # now determine nice limits by hand:
    xbinwidth = 0.5
    ybinwidth = 0.01
    xmax = np.max(np.abs(x))
    ymax = np.max(np.abs(y))
    xlim = (int(xmax / xbinwidth) + 1) * xbinwidth
    ylim = (int(ymax / ybinwidth) + 1) * ybinwidth

    xbins = np.arange(0, xlim + xbinwidth, xbinwidth)
    ybins = np.arange(0, ylim + ybinwidth, ybinwidth)
    ax_histx.hist(x, bins=xbins, align='left')
    ax_histy.hist(y, bins=ybins, orientation='horizontal')

    plt.show()
