import pandas as pd
import numpy as np
from functools import reduce

# transformer related indicators

def load_fac(total_powercurve):
    return np.mean(total_powercurve)/np.amax(total_powercurve)


def transformer_overloaded_timesteps(total_powercurve, transformer_limit):
    """
    :param total_powercurve: numpy array of power at transformer
    :param transformer_limit: int or float of rated transformer limit
    :return: # of timesteps where rated transformer limit is exceeded
    """
    total_timesteps = 0
    for power in total_powercurve:
        if power > transformer_limit:
            total_timesteps += 1
    return total_timesteps


# noinspection PyUnusedLocal
# TODO: compute avg_power really using the total_powercurve
def avg_transformer_loading(total_powercurve, transformer_limit):
    avg_power = 1
    return transformer_limit/avg_power


def max_transformer_overload(total_powerCurve, transformer_limit):
    return total_powerCurve.max() - transformer_limit


def max_transformer_power_perc(total_powerCurve, transformer_limit):
    return np.amax(total_powerCurve)/transformer_limit*100.0


def network_diagnostics(total_powerCurve, transformer_limit, overloaded_timesteps=True, max_transformer_power_pc=True,
                        load_factor=True):
    if overloaded_timesteps:
        print("# of timesteps with transformer overload: " +
              str(transformer_overloaded_timesteps(total_powerCurve, transformer_limit)))
    if max_transformer_power_pc:
        print("Max transformer power in percent: " +
              str(max_transformer_power_perc(total_powerCurve, transformer_limit)))
    if load_factor:
        print("load factor: " + str(load_fac(total_powerCurve)))


# personal peak related indicators

def personal_peak_charge(hh, network_charge):
    return network_charge/hh.inflex_power.max()


# contribution to single highest network peak indicators

def contribution_to_network_peak_inflexible(hh, network_powerCurve):
    peak_time = network_powerCurve.idxmax()
    return hh.powerCurve(peak_time)/network_powerCurve(peak_time)


def contribution_to_network_peak_ev(hh, network_powerCurve):
    peak_time = network_powerCurve.idxmax()
    return hh.ev_power(peak_time)/network_powerCurve(peak_time)


def contribution_to_network_peak_total(hh, network_powerCurve):
    return (contribution_to_network_peak_ev(hh, network_powerCurve) +
            contribution_to_network_peak_inflexible(hh, network_powerCurve))


def network_peak_contribution_charge(hh, network_powerCurve, network_charge):
    return network_charge/contribution_to_network_peak_total(hh, network_powerCurve)


# contribution to highest network power percentiles

def contribution_to_x_highest_power_percentiles(hh_power, network_powerCurve, x):
    """Function to compute the average contribution of this household to the x highest percent of network power.

    :param hh_power: time series of power of household whose contribution is to be determined. Can be only the
    inflexible power, only ev power, or sum of both.

    :param network_powerCurve: total power curve of network as pandas time series
    :param x: percentage of highest network power time steps to use

    :return: average contribution of this household to specified percentage of highest network power
    """
    n_timesteps = len(network_powerCurve.index)
    n_highest_timesteps = int(float(x)/100.0 * n_timesteps)
    highest_network_powercurve = network_powerCurve.sort_values(ascending=False).iloc[0:n_highest_timesteps]
    contribution_sum = 0
    for time in highest_network_powercurve.index:
        contribution_sum += hh_power[time]/highest_network_powercurve[time]
    avg_contribution = contribution_sum/n_highest_timesteps
    return avg_contribution


def x_highest_power_percentiles_contribution_charge(hh, network_powerCurve, x, network_charge):
    return network_charge/contribution_to_x_highest_power_percentiles(hh, network_powerCurve, x)


# volumetric computations

def total_volume(hh_power_Curve):
    return sum(hh_power_Curve)


def personal_volume_charge(hh_powerCurve, network_charge):
    """Function to compute the charge per kWh of this household

    :param hh_powerCurve: power curve of household. Can be EV power, inflexible, or sum of both.
    :param network_charge: network charge in Euro
    :return: charge per kWh
    """
    return network_charge/(sum(hh_powerCurve))


def total_tariff_cost_per_kWh(households):
    """

    :param households: dictionary of households with inflexible and ev power curves loaded
    :return: dictionary id: average tariff cost per kWh (no wholesale price included)
    """
    costs_per_kwh = dict()
    for hh in households.values():
        total_powercurve = hh.inflex_power + hh.ev_power if hh.ev else hh.inflex_power
        avg_charge_per_kwh = hh.tariff.avg_charge_per_kWh(total_powercurve)
        costs_per_kwh[hh.ident] = avg_charge_per_kwh
    print("Dictionary of average costs per kwh, including EV power: " + str(costs_per_kwh))
    return costs_per_kwh

def total_tariff_charge(households):
    """

    :param households: dictionary of households with inflexible and ev power curves loaded
    :return: dictionary id: average tariff cost per kWh (no wholesale price included)
    """
    total_tariff_charge = dict()
    for hh in households.values():
        total_powercurve = hh.inflex_power + hh.ev_power if hh.ev else hh.inflex_power
        total_tariff_charge[hh.ident] = hh.tariff.total_charge(total_powercurve)
    # print("Dictionary of total tariff charge, including EV power: " + str(total_tariff_charge))
    return total_tariff_charge