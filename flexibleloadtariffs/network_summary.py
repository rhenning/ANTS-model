from functools import reduce
import pandas as pd
import numpy as np
from flexibleloadtariffs.neighborhood import Neighborhood
from flexibleloadtariffs.simulation import config

# globals
time_step = config['time step']
time_steps_in_year = float(365*24*60/time_step)
markup_price = config['p_markup']
trafo_power_limit = config['trafo replacement fraction']
trafo_asset_cost = config['trafo asset cost']
trafo_inst_cost = config['trafo inst cost']
trafo_life_time = config['trafo life time']
trafo_life_remain = config['trafo life remain']
interest_rate = config['interest rate']
early_repl_contr_limit = config['early repl contribution limit']


# losses
def losses_ts(neighborhood: Neighborhood):
    """
    Eq. ?? in paper
    :param neighborhood: neighborhood with household and ev power curves given
    :return: time series of losses in kW per time step
    """
    loss_ts = (neighborhood.total_power_ts.pow(2)).mul(
        neighborhood.transformer_loss_factor / neighborhood.transformer_size)
    return loss_ts


def cost_losses_ts(neighborhood: Neighborhood):
    """
    eq. ?? in paper
    :param neighborhood
    :return: time series of costs of losses per time step
    """
    loss_ts = losses_ts(neighborhood)
    # return loss_ts.mul(markup_price * time_step / 60)
    return (neighborhood.price_time_series.add(markup_price)).mul(loss_ts).mul(time_step/60)


def sum_of_squares_ts(neighborhood: Neighborhood):
    squares_gen = (hh.total_power.pow(2) for hh in neighborhood.households.values())
    squares_ts = reduce(lambda x, y: x.add(y), squares_gen)
    return squares_ts


def cost_contr_loss(neighborhood: Neighborhood):
    """
    eq. 18 in paper, return dict of sum of cost contributions to losses per household
    :param cost_loss_ts:
    :param neighborhood:
    :return:
    """
    cost_contr_loss_hh = dict()
    cost_loss_ts = cost_losses_ts(neighborhood)
    neighborhood_total_power = neighborhood.total_power_ts
    # for quadratic allocation of contributions - not used anymore
    # hh_squares_sum_ts = sum_of_squares_ts(neighborhood)
    for hh in neighborhood.households.values():
        cost_contr_loss_ts = cost_loss_ts.mul(hh.total_power.div(neighborhood_total_power))
        cost_contr_loss_hh[hh.ident] = cost_contr_loss_ts.sum()
    return cost_contr_loss_hh


# fixed costs
def cost_contr_fixed(neighborhood: Neighborhood, distribution="evenly"):
    cost_contr_fixed_hh = dict()
    for hh in neighborhood.households.values():
        if distribution == "evenly":
            cost_contr_fixed_hh[hh.ident] = neighborhood.fixed_costs/float(neighborhood.number_of_households)
                                            # * \
                                            # neighborhood.total_power_ts.size/time_steps_in_year
    return cost_contr_fixed_hh


# early replacement costs
def replacement_condition(neighborhood: Neighborhood):
    """
    eq ?? in paper
    :param neighborhood:
    :return: True or False depending on whether 95% of trafo rated power is reached
    """
    replacement = False
    if neighborhood.transformer_upgrades > 0:
        replacement = True
    return replacement


def early_replacement_cost_total(neighborhood: Neighborhood):
    """
    eq ?? in paper
    :param neighborhood:
    :return:
    """
    return (trafo_life_remain/trafo_life_time) * (trafo_asset_cost + trafo_inst_cost) * neighborhood.transformer_upgrades


def early_replacement_cost_annual(neighborhood: Neighborhood):
    """
    eq ?? in paper
    :param neighborhood:
    :return:
    """
    return early_replacement_cost_total(neighborhood) * interest_rate/(1 - pow(1 + interest_rate, -trafo_life_time))


def compute_contributing_peak_times(neighborhood: Neighborhood, rule="max n", n_max=10, load_threshold=0.9):
    if rule == "max n":
        # select index list based on n largest load peaks
        contributing_times = neighborhood.total_power_ts.nlargest(n=n_max).index
        # print(contributing_times)
    elif rule == "transformer loading":
        # select index list based on trafo overloading condition
        # if the transformer has been upgraded, we use the trafo size BEFORE the last upgrade to determine peak
        # contributions, in case of no upgrade we simply use the initial transformer size
        considered_trafo_size_peaks = (neighborhood.transformer_size / neighborhood.transformer_upgrade_factor) if bool(
            neighborhood.transformer_upgrades) else neighborhood.transformer_size
        contributing_times = neighborhood.total_power_ts.loc[neighborhood.total_power_ts.values >
                                                             considered_trafo_size_peaks * load_threshold].index
    else:
        print("Unknown allocation rule. No contributing peak times calculated.")
        contributing_times = None
    return contributing_times

def cost_contr_replacement(neighborhood: Neighborhood, contributing_times):
    """
    eq ?? in paper
    :param neighborhood:
    :return:
    """
    cost_contr_repl_hh = dict()
    repl_cost = early_replacement_cost_annual(neighborhood)
    # replacement = False
    # if replacement_condition(neighborhood):
    #     replacement = True
    for hh in neighborhood.households.values():
        if bool(repl_cost) and len(contributing_times) > 0:
            avg_contr = ((hh.total_power.loc[contributing_times]).div(
                neighborhood.total_power_ts.loc[contributing_times])).mean()
            # print("average peak contribution: " + str(avg_contr))
            cost_contr_repl_hh[hh.ident] = avg_contr * early_replacement_cost_annual(neighborhood)
                                       #    * neighborhood.total_power_ts.size/time_steps_in_year
        else:
            cost_contr_repl_hh[hh.ident] = 0
    return cost_contr_repl_hh


def cost_contr_total(neighborhood: Neighborhood, contributing_times):
    cost_contr_repl_dict = cost_contr_replacement(neighborhood, contributing_times)
    cost_contr_fixed_dict = cost_contr_fixed(neighborhood)
    cost_contr_loss_dict = cost_contr_loss(neighborhood)
    cost_contr_total_hh = dict()
    for hh in neighborhood.households.values():
        cost_contr_total_hh[hh.ident] = cost_contr_repl_dict[hh.ident] + cost_contr_fixed_dict[hh.ident] + \
                                        cost_contr_loss_dict[hh.ident]
    return cost_contr_total_hh


def compute_cost_correlations(neighborhood: Neighborhood, contributing_times):
    cost_contr = cost_contr_total(neighborhood, contributing_times)
    x_ev = np.fromiter((hh.tariff.total_charge(hh.total_power) for hh in neighborhood.households.values()
                        if hh.ev is not None),
                       float)
    y_ev = np.fromiter((cost_contr[hh.ident] for hh in neighborhood.households.values() if hh.ev is not None),
                       float)
    x_no_ev = np.fromiter((hh.tariff.total_charge(hh.total_power) for hh in neighborhood.households.values()
                           if hh.ev is None),
                          float)
    y_no_ev = np.fromiter((cost_contr[hh.ident] for hh in neighborhood.households.values() if hh.ev is None),
                          float)
    x_all = np.fromiter((hh.tariff.total_charge(hh.total_power) for hh in neighborhood.households.values()),
                        float)
    y_all = np.fromiter((cost_contr[hh.ident] for hh in neighborhood.households.values()),
                        float)
    cost_corr = {"No EV": np.corrcoef(x_no_ev, y_no_ev)[0][1], "With EV": np.corrcoef(x_ev, y_ev)[0][1],
                 "All": np.corrcoef(x_all, y_all)[0][1]}
    # print(cost_corr)
    return cost_corr


def compute_nonfixed_cost_contributions(neighborhood: Neighborhood, contributing_times):
    cost_contr_losses = cost_contr_loss(neighborhood)
    cost_contr_repl = cost_contr_replacement(neighborhood, contributing_times)
    cost_contr_all = sum(cost_contr_losses[hh.ident] + cost_contr_repl[hh.ident]
                         for hh in neighborhood.households.values())
    # print("non-fixed cost contribution: " + str(cost_contr_all))
    return cost_contr_all

def ev_charging_cost_daprice(neighborhood: Neighborhood):
    daprice_charging_cost =dict()
    total_cost = 0
    for hh in neighborhood.households.values():
        daprice_charging_cost[hh.ident] = (hh.ev_power.mul(neighborhood.price_time_series).mul(time_step/60)).sum()
        total_cost += daprice_charging_cost[hh.ident]
    return daprice_charging_cost, total_cost


def network_peak(neighborhood: Neighborhood, initial_trafo_size: float):
    peak_time = neighborhood.total_power_ts.idxmax()
    peak_size = neighborhood.total_power_ts.loc[peak_time]/initial_trafo_size
    EV_peak_contr = neighborhood.total_ev_power_ts.loc[peak_time]/initial_trafo_size
    Non_EV_peak_contr = neighborhood.total_inflex_power_ts.loc[peak_time]/initial_trafo_size
    return peak_size, EV_peak_contr, Non_EV_peak_contr


def load_factor(neighborhood: Neighborhood):
    return neighborhood.total_power_ts.mean()/neighborhood.total_power_ts.max()


