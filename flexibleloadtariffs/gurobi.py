import gurobipy as gp
from gurobipy import GRB
from datetime import datetime
from functools import reduce
import pandas as pd

# globals
# TODO: read from inputs
time_step = 15
conn_limit = 17.0


# helper functions
def _create_timestamp_dict(load_ts):
    # set up an index to timestamp dictionary from the given time series
    return dict(enumerate(load_ts.index))


def _is_time_between(begin_time, end_time, check_time):
    if begin_time < end_time:
        return begin_time <= check_time <= end_time
    else:  # crosses midnight
        return check_time >= begin_time or check_time <= end_time


# Gurobi optimization models
def grb_scheduling_model(loadcurves_dict, ev_const_dict, strategy: str, da_price_ts: pd.Series,
                         transformer_limit = 999):
    """
    creates a basic model with initialization of the indexes and all the minimal constraints:
    maximal charger throughput, battery size, charge at departure, state of charge, charging availability.
    :param loadcurves_dict: dictionary of household load curves
    :param ev_const_dict: dictionary of household ev constraints, should have the same keys as
    loadcurves_dict
    :param strategy: charging strategy - central optimization (opt-cen) or individual optimization (opt-ind)
    :param da_price_ts: day ahead price time series
    :param transformer_limit: rated power of transformer
    :return: ev load dictionary for existing ev constraints in the ev constraint dict
    """
    # model initialization
    scheduling_model = gp.Model('EV Scheduling Model')

    # create a dict of time indexes to datetime objects from load time series
    total_load_ts = reduce(lambda x, y: x.add(y), loadcurves_dict.values())
    timestamp_dict = _create_timestamp_dict(total_load_ts)

    # declare index sets for EV's and time
    ev_index_set = [key for key in ev_const_dict.keys() if ev_const_dict[key] is not None]
    # scheduling_model.ev_ind = pyo.Set(initialize=[key for key in ev_const_dict.keys() if ev_const_dict[key]
    #                                         is not None])
    time_index_set = timestamp_dict.keys()
    # scheduling_model.time_ind = pyo.Set(initialize=timestamp_dict.keys())

    # declare the optimization variable, charge, indexed by time and ev
    charge_power = scheduling_model.addVars(time_index_set, ev_index_set, name="avg power over time step")
    # scheduling_model.charge = pyo.Var(scheduling_model.time_ind, scheduling_model.ev_ind, domain=pyo.NonNegativeReals)
    # declare a state variable, state of charge of battery
    state_of_charge = scheduling_model.addVars(time_index_set, ev_index_set, name="battery charge in kWh")
    # scheduling_model.stateOfCharge = pyo.Var(scheduling_model.time_ind, scheduling_model.ev_ind, domain=pyo.NonNegativeReals)

    # minimal constraints
    # charge is bounded by max charge rate for each EV at any time, or by the household connection limit
    # (assumed to be 17 kW by default)
    max_power = scheduling_model.addConstrs(
        (
            charge_power[time_index, ev_index] <= min(
                conn_limit - float(loadcurves_dict[ev_index].loc[timestamp_dict[time_index]]),
                ev_const_dict[ev_index].max_charge)
            for time_index in time_index_set for ev_index in ev_index_set
        ),
        name="maximal power")

    # def max_charge_rule(model, time, ev_i):
    #     return model.charge[time, ev_i] <= min(conn_limit - float(loadcurves_dict[ev_i].loc[timestamp_dict[time]]),
    #                                            ev_const_dict[ev_i].max_charge)
    #
    # scheduling_model.maxChargeConstraint = pyo.Constraint(scheduling_model.time_ind, scheduling_model.ev_ind, rule=max_charge_rule)

    # state of charge is bounded by battery size for each EV at any time
    battery_size = scheduling_model.addConstrs(
        (
            state_of_charge[time_index, ev_index] <= ev_const_dict[ev_index].battery_size
            for time_index in time_index_set for ev_index in ev_index_set
        ),
        name="maximal state of charge")

    # def battery_size_rule(model, time, ev_i):
    #     return model.stateOfCharge[time, ev_i] <= ev_const_dict[ev_i].battery_size
    #
    # scheduling_model.BatteryConstraint = pyo.Constraint(scheduling_model.time_ind, scheduling_model.ev_ind, rule=battery_size_rule)

    # charge requirement for each EV each morning at departure time:
    departure_charge = scheduling_model.addConstrs(
        (
            state_of_charge[time_index, ev_index] >= ev_const_dict[ev_index].target_charge
            for time_index in time_index_set for ev_index in ev_index_set
            if datetime.time(timestamp_dict[time_index]) == ev_const_dict[ev_index].departure_time
        ),
        name="Charge requirement at departure")

    # def charge_at_departure_rule(model, time, ev_i):
    #     ev = ev_const_dict[ev_i]
    #     # if time is departure time, set constraint at target state of charge percentage
    #     if datetime.time(timestamp_dict[time]) == ev.departure_time:
    #         return model.stateOfCharge[time, ev_i] >= ev.target_charge
    #     # if time is not departure time: no action needed
    #     return pyo.Constraint.Skip
    #
    # scheduling_model.charge_at_departure = pyo.Constraint(scheduling_model.time_ind, scheduling_model.ev_ind,
    #                                                 rule=charge_at_departure_rule)

    # initial charges
    initial_charges = scheduling_model.addConstrs(
        (
            state_of_charge[0, ev_index] == ev_const_dict[ev_index].current_charge
            for ev_index in ev_index_set
        ),
        name="Initial charges")

    # relate state of charge to charge in previous time step
    charge_update = scheduling_model.addConstrs(
        (
            state_of_charge[time_index, ev_index] ==
            state_of_charge[time_index - 1, ev_index]
            + (charge_power[time_index - 1, ev_index]
               * ev_const_dict[ev_index].charging_efficiency * time_step/60)
            for time_index in time_index_set[1:] for ev_index in ev_index_set
        ),
        name="Charge update rule")

    # reduce charge by daily demand every day at arrival time
    daily_demand_charge = scheduling_model.addConstrs(
        (
            state_of_charge[time_index, ev_index] ==
            state_of_charge[time_index - 1, ev_index]
            - ev_const_dict[ev_index].daily_demand
            for time_index in time_index_set[1:] for ev_index in ev_index_set
            if datetime.time(timestamp_dict[time_index]) == ev_const_dict[ev_index].arrival_time
        ),
        name="Daily demand charge reduction")

    # def charge_update_rule(model, time, ev_i):
    #     ev = ev_const_dict[ev_i]
    #     # set initial charges to current state of charge of the battery
    #     if time == 0:
    #         return model.stateOfCharge[time, ev_i] == ev.current_charge
    #     # get index for previous time_step
    #     prev_time_step = time - 1
    #     # reduce state of charge by daily demand every day on arrival
    #     if datetime.time(timestamp_dict[time]) == ev.arrival_time:
    #         return model.stateOfCharge[time, ev_i] == model.stateOfCharge[prev_time_step, ev_i] \
    #                - ev.daily_demand
    #     # dynamically update state of charge afterwards, based on SOC in previous time step
    #     return model.stateOfCharge[time, ev_i] == model.stateOfCharge[prev_time_step, ev_i] \
    #         + ev.charging_efficiency * model.charge[prev_time_step, ev_i] * time_step/60
    # 
    # scheduling_model.SOC_constraint = pyo.Constraint(scheduling_model.time_ind, scheduling_model.ev_ind, rule=charge_update_rule)

    # # charging is only possible between arrival and departure time
    home_charge = scheduling_model.addConstrs(
        (
            charge_power[time_index, ev_index] == 0
            for time_index in time_index_set for ev_index in ev_index_set
            if not _is_time_between(ev_const_dict[ev_index].arrival_time,
                                    ev_const_dict[ev_index].departure_time,
                                    datetime.time(timestamp_dict[time_index]))
        ),
        name="Home charge requirement")

    # def home_charge_rule(model, time, ev_i):
    #     ev = ev_const_dict[ev_i]
    #     arrival_time = ev.arrival_time
    #     departure_time = ev.departure_time
    #     current_time = datetime.time(timestamp_dict[time])
    #     if not _is_time_between(arrival_time, departure_time, current_time):
    #         return model.charge[time, ev_i] == 0
    #     return pyo.Constraint.Skip
    #
    # scheduling_model.HomeChargeConstraint = pyo.Constraint(scheduling_model.time_ind, scheduling_model.ev_ind,
    
    #                                                  rule=home_charge_rule)
    
    # central optimization specific
    if strategy == 'opt-cen':
        trafo_limit = scheduling_model.addConstrs(
            (
                charge_power.sum(time_index, '*')
                + float(total_load_ts.loc[timestamp_dict[time_index]])
                <= transformer_limit - 0.1
                for time_index in time_index_set
            ),
            name = "Transformer power limit")

        # def transformer_constraint_rule(model, time):
        #     return sum(model.charge[time, ev_i] for ev_i in model.ev_ind) \
        #            + float(total_load_ts.loc[idx_to_timestamp_dict[time]]) <= transformer_limit - 0.1
        #
        # central_model.transformer_constraint = pyo.Constraint(central_model.time_ind, rule=transformer_constraint_rule)

        central_objective = gp.quicksum(
            float(da_price_ts.loc[timestamp_dict[time_index]])
            * scheduling_model.charge[time_index, ev_index]
            for time_index in time_index_set for ev_index in ev_index_set
        )

        scheduling_model.setObjective(central_objective, GRB.MINIMIZE)

        scheduling_model.optimize()

    # write ev load outputs
    ev_load_dict = dict()
    for ev_index in ev_index_set:
        ev_load_ts = pd.Series(0, index=total_load_ts.index)
        for time_index in time_index_set:
            ev_load_ts.iloc[time_index] = scheduling_model.charge[time_index, ev_index]
        ev_load_dict[ev_index] = ev_load_ts
    
    return ev_load_dict

    # central objective function, minimizing charging costs across all EV's at once
    # def obj_expression(model):
    #     return sum(float(da_price_ts.loc[idx_to_timestamp_dict[h]]) * model.charge[h, ev_i]
    #                for h in model.time_ind for ev_i in model.ev_ind)
    #
    # central_model.OBJ = pyo.Objective(rule=obj_expression)

    # solve model
    # opt = pyo.SolverFactory('glpk')
    # opt.solve(central_model)
    #
    # ev_load = dict()
    # for hh_ind in central_model.ev_ind:
    #     ev_load[hh_ind] = _pyomo_output_to_ev_load_ts(total_load_ts.index, idx_to_timestamp_dict,
    #                                                   central_model, hh_ind)
    #
    # return ev_load