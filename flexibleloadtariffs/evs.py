import pandas as pd
import numpy as np
from datetime import datetime
from math import ceil
from pathlib import Path
from flexibleloadtariffs.performance import max_transformer_power_perc
from flexibleloadtariffs.utils.random_draws import draw_time, draw_soc, draw_daily_demand
from flexibleloadtariffs.utils.plots import variable_ev_transformer_power_plot
from flexibleloadtariffs.simulation import config

# globals
time_step = config['time step']
reporoot_dir = Path(__file__).resolve().parent.parent


class EV:
    def __init__(self, ev_id, arrival_time_mean, arrival_time_std, departure_time, current_soc_perc,
                 start_soc_std, target_soc_perc, battery_size, max_charge, charging_efficiency,
                 daily_demand_mean, daily_demand_std, tariff=None):
        self.ev_id = ev_id
        self.arrival_time = datetime.time(draw_time(arrival_time_mean, arrival_time_std, time_step))
        # using draw_time to round departure time up to the nearest time step for now, a bit of a quit hack
        # should round down to the nearest time step for departure time. Should do this when rearranging modules
        self.departure_time = datetime.time(draw_time(departure_time, 0, time_step))
        self.target_soc_perc = target_soc_perc
        self.target_charge = target_soc_perc*float(battery_size)
        self.battery_size = battery_size
        self.max_charge = max_charge
        self.charging_efficiency = charging_efficiency
        self.daily_demand = draw_daily_demand(daily_demand_mean, daily_demand_std,
                                              self.target_soc_perc * self.battery_size)
        # initialize current charges at target demand - daily demand:
        self.current_charge = self.target_charge - self.daily_demand
        self.current_soc_perc = self.current_charge/battery_size
        # if EVs can have a different tariff than households, specify tariff here:
        self.tariff = tariff


def load_ev_constraints(inputs, households):
    """Loads EV constraints from the file specified in inputs
    and randomly assigns them to the list of specified households.

    When there are fewer EV's in the constraints file than number of EV's (n_ev) specified in the input file,

    :param inputs: yaml file with path to EV constraints file
    :param households: (dictionary) of households with id's as keys
    :return: (dictionary) of households with EV's assigned. If EV's were assigned before already, these are deleted
    before the new ones are assigned.
    """

    n_ev = int(inputs['n_ev'])
    ev_constraint_path = inputs['EV_constraint_path']
    arrival_time = inputs['arrival_time']
    arrival_time_std = inputs['arrival_time_std']
    target_time = inputs['departure_time']
    start_soc_perc_mean = inputs['start_soc_mean']
    start_soc_perc_std = inputs['start_soc_std']
    target_soc_perc = inputs['target_soc']
    battery_size = inputs['battery_size']
    max_charge = inputs['max_charge']
    charging_efficiency = inputs['charging_efficiency']
    daily_demand_mean = inputs['daily_demand_mean']
    daily_demand_std = inputs['daily_demand_std']
    ev_constraint_df = pd.read_csv(reporoot_dir / ev_constraint_path, sep=';')
    # randomly select households which get an EV
    hh_indices = np.random.choice(inputs['n_hh'], int(inputs['n_ev']), replace = False)
    # get number of ev profiles specified in the inputs file
    n_ev_profiles = len(ev_constraint_df.index)
    # if number of ev's is bigger than the number of EV profiles we have, we have to draw some of them multiple times
    multiplier = ceil(n_ev/n_ev_profiles)
    # randomly select EVs from EV profiles
    ev_indices = np.random.choice(n_ev_profiles*multiplier, n_ev, replace = False)
    for n, hh_index in enumerate(hh_indices):
        this_ev_index = ev_indices[n]
        # print("this ev index: " + str(this_ev_index))
        row = ev_constraint_df.iloc[this_ev_index, :]
        households[hh_index].ev = \
            EV(n+1, pd.to_datetime(row[arrival_time]), row[arrival_time_std],
               pd.to_datetime(row[target_time], format='%H:%M'), float(row[start_soc_perc_mean]),
               float(row[start_soc_perc_std]), float(row[target_soc_perc]), float(row[battery_size]),
               float(row[max_charge]), float(row[charging_efficiency]), float(row[daily_demand_mean]),
               float(row[daily_demand_std])
               )
    return households


# variable ev runs
# TODO needs to be updated since charging strategies were refactored
def variable_ev_runs(households, inputs, da_price_ts, total_power_np_array, plotting=True):
    max_power_dict = {}

    # list of the implemented charging strategies: IO - individual optimization, CoA - Charge on Arrival
    # unconstrained - without capacity bandwidth, cap. sub. - with capacity bandwidth
    charge_strategies = ['IO unconstrained', 'IO + cap. sub.', 'CoA unconstrained', 'CoA + cap. sub.']

    # number of random trials to average over
    n_trials = inputs['n_trials']

    start_time = pd.to_datetime(inputs['start_time'], format=inputs['datetime format'])
    end_time = pd.to_datetime(inputs['end_time'], format=inputs['datetime format'])
    time_step = inputs['time_step']
    min_ev = inputs['min_ev']
    max_ev = inputs['max_ev']
    step_ev = inputs['step_ev']
    ev_index = np.arange(min_ev, max_ev + 1, step_ev).astype(int)

    for charge_strategy in charge_strategies:
        # initialize a dictionary that holds lists of the maximal transformer power for each EV number
        # for each charging strategy
        max_power_dict[charge_strategy] = np.zeros(ceil((max_ev + 1 - min_ev) / step_ev))

    for ev_number in ev_index:
        print("Current EV number: " + str(ev_number))
        this_ev_number_max_power = {}
        # CAREFUL!!!!! THIS OVERWRITES AN INPUT PARAMETER - n_ev
        inputs['n_ev'] = ev_number
        for charge_strategy in charge_strategies:
            # initialize a dictionary that holds numpy arrays of the maximal transformer power for each trial with
            # this EV number for each charging strategy
            this_ev_number_max_power[charge_strategy] = np.zeros(n_trials)

        trial = 0
        while trial < n_trials:
            # sometimes random draws of EV charging requirements can not be fulfilled within bandwidth.
            # in the future, I should find a more elegant solution for how to handle this case, but for now
            # the trial is just discarded and new EV charging requirements drawn.
            try:
                load_ev_constraints(inputs, households)
                for charge_strategy in charge_strategies:
                    # TODO: look at how to better express all the possibilities here (instead of if-else with strings)
                    total_ev_power = 0  # Should never be this value because one of the options will be chosen
                    # computations for all specified charging strategies
                    if 'unconstrained' in charge_strategy:
                        bw = False
                    else:
                        bw = True
                    if 'IO' in charge_strategy:
                        total_ev_power = strat_individual(households, da_price_ts, start_time, end_time,
                                                         with_bw=bw)
                    elif 'CoA' in charge_strategy:
                        total_ev_power = strat_coa(households, start_time, end_time, with_bw=bw)
                    total_combined_power = total_power_np_array + total_ev_power
                    max_power_perc = max_transformer_power_perc(total_combined_power, inputs['PT_max_kW'])
                    this_ev_number_max_power[charge_strategy][trial] = max_power_perc
                trial += 1
            except TypeError:
                print("at least one charge constraint can not be satisfied in the available bandwidth. \
                      Repeating trial")
                pass

        for charge_strategy in charge_strategies:
            # update the dictionary of maximal transformer powers for each EV number with the average
            # of maximal power over all random trials for this EV number
            max_power_dict[charge_strategy][int((ev_number - min_ev) / step_ev)] = \
                np.mean(this_ev_number_max_power[charge_strategy])

    if plotting:
        variable_ev_transformer_power_plot(max_power_dict, ev_index)

    return max_power_dict
