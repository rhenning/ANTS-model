from datetime import datetime, timedelta
from functools import reduce
import pyomo.environ as pyo
import pandas as pd
from flexibleloadtariffs.simulation import config

# globals
time_step = config['time step']
conn_limit = config['household conn limit']
with_bw = config['bw on']
system_messages = config['system messages']


# helper functions
def _create_timestamp_dict(power_ts):
    # set up an index to timestamp dictionary from the given time series
    return dict(enumerate(power_ts.index))


def _is_time_between(begin_time, end_time, check_time):
    if begin_time < end_time:
        return begin_time <= check_time <= end_time
    else:  # crosses midnight
        return check_time >= begin_time or check_time <= end_time


# scheduling functions
# central
def central_optimization_model(households, da_price_ts, start_time, end_time, transformer_limit):
    """
    central optimization model that has as an objective to minimize cost for satisfying all EV constraints and 
    respects network constraints.
    :param households: dictionary of households for scheduling
    :param ev_const_dict: dictionary of ev constraints with household index as key
    :param transformer_limit: rated power of transformer
    :param da_price_ts: day ahead price time series
    :return: solved PYOMO model
    """
    # initialize model
    # TODO this computation of total power series should be done only once outside of this model scope to save time
    powercurves_dict = {hh.ident: hh.inflex_power.loc[start_time:end_time] for hh in households.values()}
    total_power_ts = reduce(lambda x, y: x.add(y), powercurves_dict.values())
    idx_to_timestamp_dict = _create_timestamp_dict(total_power_ts)
    ev_const_dict = {hh.ident: hh.ev for hh in households.values()}

    # model initialization
    central_model = pyo.ConcreteModel()
    # declare index sets for EV's and time
    central_model.ev_ind = pyo.Set(initialize=[key for key in ev_const_dict.keys() if ev_const_dict[key]
                                            is not None])
    central_model.time_ind = pyo.Set(initialize=idx_to_timestamp_dict.keys())

    # declare the optimization variable, charge, indexed by time and ev
    central_model.charge = pyo.Var(central_model.time_ind, central_model.ev_ind, domain=pyo.NonNegativeReals)
    # declare a state variable, state of charge of battery
    central_model.stateOfCharge = pyo.Var(central_model.time_ind, central_model.ev_ind, domain=pyo.NonNegativeReals)

    # general constraints
    # charge is bounded by max charge rate for each EV at any time, or by the household connection limit
    # (assumed to be 17 kW by default)
    def max_charge_rule(model, time, ev_i):
        return model.charge[time, ev_i] <= min(conn_limit -
                                               float(powercurves_dict[ev_i].loc[idx_to_timestamp_dict[time]]),
                                               ev_const_dict[ev_i].max_charge)

    central_model.maxChargeConstraint = pyo.Constraint(central_model.time_ind, central_model.ev_ind,
                                                       rule=max_charge_rule)

    # state of charge is bounded by battery size for each EV at any time
    def battery_size_rule(model, time, ev_i):
        return model.stateOfCharge[time, ev_i] <= ev_const_dict[ev_i].battery_size

    central_model.BatteryConstraint = pyo.Constraint(central_model.time_ind, central_model.ev_ind,
                                                     rule=battery_size_rule)

    # charge requirement for each EV each morning at departure time:
    def charge_at_departure_rule(model, time, ev_i):
        ev = ev_const_dict[ev_i]
        # if time is departure time, set constraint at target state of charge percentage
        if datetime.time(idx_to_timestamp_dict[time]) == ev.departure_time:
            return model.stateOfCharge[time, ev_i] >= ev.target_charge
        # if time is not departure time: no action needed
        return pyo.Constraint.Skip

    central_model.charge_at_departure = pyo.Constraint(central_model.time_ind, central_model.ev_ind,
                                                    rule=charge_at_departure_rule)

    # relate state of charge to charging and depletion of daily demand
    def charge_update_rule(model, time, ev_i):
        ev = ev_const_dict[ev_i]
        # set initial charges to current state of charge of the battery
        if time == 0:
            return model.stateOfCharge[time, ev_i] == ev.current_charge
        # get index for previous time_step
        prev_time_step = time - 1
        # reduce state of charge by daily demand every day on arrival
        if datetime.time(idx_to_timestamp_dict[time]) == ev.arrival_time:
            return model.stateOfCharge[time, ev_i] == model.stateOfCharge[prev_time_step, ev_i] \
                   - ev.daily_demand
        # dynamically update state of charge afterwards, based on SOC in previous time step
        return model.stateOfCharge[time, ev_i] == model.stateOfCharge[prev_time_step, ev_i] \
               + ev.charging_efficiency * model.charge[prev_time_step, ev_i] * time_step / 60

    central_model.SOC_constraint = pyo.Constraint(central_model.time_ind, central_model.ev_ind,
                                                  rule=charge_update_rule)

    # # charging is only possible between arrival and departure time
    def home_charge_rule(model, time, ev_i):
        ev = ev_const_dict[ev_i]
        arrival_time = ev.arrival_time
        departure_time = ev.departure_time
        current_time = datetime.time(idx_to_timestamp_dict[time])
        if not _is_time_between(arrival_time, departure_time, current_time):
            return model.charge[time, ev_i] == 0
        return pyo.Constraint.Skip

    central_model.HomeChargeConstraint = pyo.Constraint(central_model.time_ind, central_model.ev_ind,
                                                     rule=home_charge_rule)
    
    # central optimization specific constraints

    # transformer requirement for each hour, subtracting 0.1 as safety margin
    def transformer_constraint_rule(model, time):
        return sum(model.charge[time, ev_i] for ev_i in model.ev_ind) \
               + float(total_power_ts.loc[idx_to_timestamp_dict[time]]) <= transformer_limit - 0.1

    central_model.transformer_constraint = pyo.Constraint(central_model.time_ind, rule=transformer_constraint_rule)

    # central objective function, minimizing charging costs across all EV's at once
    def obj_expression(model):
        return sum(float(da_price_ts.loc[idx_to_timestamp_dict[h]]) * model.charge[h, ev_i]
                   for h in model.time_ind for ev_i in model.ev_ind)

    central_model.OBJ = pyo.Objective(rule=obj_expression)
    
    # solve model
    opt = pyo.SolverFactory('glpk')
    opt.solve(central_model)

    for hh_ind in central_model.ev_ind:
        if not households[hh_ind].ev is None:
            ev_power = pd.Series(data=[central_model.charge[t, hh_ind]() for t in central_model.time_ind],
                                index=total_power_ts.index)
            yield hh_ind, ev_power


# individual models models
def _schedule():
    pass

def opt_schedule_ev_single_day(hh, start_dtime: datetime, end_dtime: datetime, sorted_da_prices):
    hh_inflex_power = hh.inflex_power.loc[start_dtime:end_dtime]
    ev_power_scheduled = pd.Series(0, index=hh_inflex_power.index)
    ev = hh.ev
    capacity_limit_by_month = hh.capacity_limit_by_month
    req_charge = ev.target_charge - ev.current_charge
    for time_ind in sorted_da_prices:
        if _is_time_between(ev.arrival_time, ev.departure_time, datetime.time(time_ind)):
            # max with zero is done to avoid vehicle-to-grid, which could be implemented if this is to be studied
            ev_power_this_period = max(0, min(capacity_limit_by_month[time_ind.month] - hh_inflex_power.loc[time_ind],
                                              ev.max_charge,
                                              req_charge / ev.charging_efficiency * 60 / time_step))
            ev_power_scheduled.loc[time_ind] += ev_power_this_period
            # print("req charge: " + str(req_charge) + "kWh")
            # print("charging this time step: " + str(ev_power_this_period) + "kW")
            # print("inflexible load: " + str(hh_inflex_power.loc[time_ind]) + "kW")
            req_charge -= ev_power_this_period * ev.charging_efficiency * time_step /60
        if req_charge <= 0:
            return ev_power_scheduled
    # exception for case where scheduling within the normal capacity limit is not possible.
    # TODO: a different solution is required for peak tariffs: the peak should be increased incrementally
    for time_ind in sorted_da_prices:
        if _is_time_between(ev.arrival_time, ev.departure_time, datetime.time(time_ind)):
            # now using household connection limit instead of previous capacity limit from tariff
            ev_power_this_period = min(conn_limit - hh_inflex_power.loc[time_ind] - ev_power_scheduled.loc[time_ind],
                                       ev.max_charge - ev_power_scheduled.loc[time_ind],
                                       req_charge / ev.charging_efficiency * 60 / time_step)
            ev_power_scheduled.loc[time_ind] += ev_power_this_period
            print("extra charge")
            print("charging this time step: " + str(ev_power_this_period))
            req_charge -= ev_power_this_period * ev.charging_efficiency * time_step / 60
        if req_charge <= 0:
            return ev_power_scheduled
    if req_charge > 0:
        print("Ev charging not possible within household connection limit.")


def _schedule_ev_power(hh, time_prioritization, start_dtime: datetime, end_dtime: datetime,
                      ev_power_previously_scheduled=None, in_bw_charging=with_bw):
    # function that performs the power scheduling for a single given household and time prioritization.
    # CoA prioritizes times in simple ascending order after arrival, opt-ind prioritizes times based on lowest prices
    # between arrival and departure time

    start_day = start_dtime.day
    start_time = datetime.time(start_dtime)
    # print("EV {} target charge: {}".format(hh.ev.ev_id, hh.ev.target_charge))
    # print("current charge: {}".format(hh.ev.current_charge))
    def datetime_to_night(current_dt: datetime):
        """
        this function computes how many nights have passed since start time
        :param current_dt:
        :return:
        """
        night = (current_dt - start_dtime).days
        return night
        # if datetime.time(current_dt) >= start_time:
        #      return night
        # else:   # everything up to start time of the new day still belongs to the same night
        #      return night - 1

    # add one time step for computation of number of nights
    n_nights = datetime_to_night(end_dtime + timedelta(hours=0, minutes=time_step))
    # print(n_nights)
    # print(start_dtime, end_dtime, n_nights)
    hh_temp_powercurve = hh.inflex_power.loc[start_dtime:end_dtime]
    ev = hh.ev
    capacity_limit_by_month = hh.capacity_limit_by_month
    previous_demand_scheduled = 0
    previous_energy_charged = 0
    ev_power_scheduled = pd.Series(0, index=hh_temp_powercurve.index)
    if ev_power_previously_scheduled is not None:
        # if scheduled ev power is passed in it means we have already done the first scheduling for the minimum
        # requirement (one daily demand), thus we load the previously scheduled demand and decrement the
        # required daily demands by 1
        previous_demand_scheduled = 1
        ev_power_scheduled.update(ev_power_previously_scheduled)
        previous_energy_charged = ev_power_previously_scheduled.sum()
    # extra demands for optimal forward looking charges beyond one day
    # only applied when demand was previously scheduled
    desired_extra_energy = (n_nights - 1) * previous_demand_scheduled * ev.daily_demand
    # print("desired extra energy: "+str(desired_extra_energy))
    req_charge = {}
    for night in range(n_nights):
        # required charge - is limited by maximal battery capacity of ev, where we add one kW
        # times time step in energy as a safety margin to avoid overloading
        # and also reduce required charge by one daily demand if it has been previously scheduled
        # for multiple day horizon optimization, we need to reflect that in every subsequent night, we can charge up to
        # one additional daily demand more
        # print("night: " + str(night))
        req_charge[night] = max(min((ev.target_charge + desired_extra_energy -
                                    ev.current_charge - previous_energy_charged),
                                    (ev.battery_size + night * ev.daily_demand -
                                    ev.current_charge - previous_energy_charged)),
                                0)
        # print("req charge this night: {}".format(req_charge[night]))
        if req_charge[night] <= 0:
            # if required charge has been met through previous charging we do not need to add any
            return ev_power_scheduled, True
    for time_ind in time_prioritization:
        current_night = datetime_to_night(time_ind)
        if _is_time_between(ev.arrival_time, ev.departure_time, datetime.time(time_ind)):
            if in_bw_charging:
                # can only add power if no power has been previously scheduled
                if ev_power_scheduled.loc[time_ind] == 0:
                    # remaining charge is reduced by what's possible to charge within capacity limit and at most
                    # to battery size limit
                    ev_power_this_period = max(0, min(capacity_limit_by_month[time_ind.month] -
                                              float(hh_temp_powercurve.loc[time_ind]), ev.max_charge,
                                               req_charge[current_night]/ev.charging_efficiency*60/time_step))
                else:
                    ev_power_this_period = 0
            else:
                charge_previously_scheduled = ev_power_scheduled.loc[time_ind]
                # remaining charge is reduced based on maximal charge power or household connection limit
                ev_power_this_period = min(float(ev.max_charge),
                                          conn_limit -
                                          float(hh_temp_powercurve.loc[time_ind]) - charge_previously_scheduled,
                                           req_charge[current_night] / ev.charging_efficiency * 60 / time_step)
            ev_power_scheduled.loc[time_ind] += ev_power_this_period
            for night in range(n_nights):
                if night >= current_night:
                    req_charge[night] -= ev_power_this_period * time_step * ev.charging_efficiency / 60
                    if req_charge[night] <= 0.0000001:
                        return ev_power_scheduled, True
    # if req_charge[0] > 0 and system_messages:
    #     if n_nights == 1:
    #         print("Cannot fulfill required minimum charge demand inside of capacity limit for household "
    #               + str(hh.ident))
    #     else:
    #         print("Cannot fulfill desired multi-day charge demand inside of capacity limit for household "
    #               + str(hh.ident))
    return ev_power_scheduled, False


def individual_scheduling(households: dict, start_time: datetime, end_time_update: datetime, strategy: str,
                          da_price_ts, end_time_opt=None, with_forecasts = False):
    """
    Function for households scheduling their EV individually, either charging on arrival or optimizing using day-ahead
    prices.
    :param households: Dictionary of households
    :param start_time: start time for scheduling
    :param end_time_update: end time for scheduling
    :param strategy: Charge on arrival (charge-arr) or individual optimization (opt-ind)
    :param da_price_ts: day-ahead price time series, has to include the same datetimes as household powercurves
    :param end_time_opt: optional for individual optimization, a longer term time horizon to consider for multi-day
    optimization.
    :return: schedule of ev charging from start_time to end_time_update.
    """
    time_index_forecast = da_price_ts.loc[start_time:end_time_opt].index
    da_time_index = da_price_ts.loc[start_time:end_time_update].index
    arbitrary_hh = next(iter(households.values()))
    if with_forecasts:
        pass
    if strategy == 'opt-ind':
        if arbitrary_hh.tariff.volumetric_rule is not None and \
                len(arbitrary_hh.tariff.volumetric_rule.start_times) > 1:
            # in this case there are multiple different volumetric charges, depending on the time
            # so we add the volumetric charges to the day ahead prices
            sorted_prices_forecast = da_price_ts.loc[start_time: end_time_opt].add(
                                                    arbitrary_hh.tariff.volumetric_rule.charges_to_ts(time_index_forecast)
                                                    ).sort_values().index
            da_time_prioritization = da_price_ts.loc[start_time: end_time_update].add(
                                                    arbitrary_hh.tariff.volumetric_rule.charges_to_ts(da_time_index)
                                                    ).sort_values().index
        else:
            # note: current sorting randomizes the order for same price, in particular since there's always the same price
            # for the whole hour, the 4 15-min intervals appear in random order
            da_time_prioritization = da_price_ts.loc[start_time: end_time_update].sort_values().index
            sorted_prices_forecast = da_price_ts.loc[start_time: end_time_opt].sort_values().index
    else:
        assert strategy == 'charge-arr'
        da_time_prioritization = arbitrary_hh.inflex_power[start_time: end_time_update].index
    for hh in households.values():
        if hh.ev is None:
            # if system_messages:
            #     print("no ev for household {}".format(hh.ident))
            continue

        ev_power_scheduled = opt_schedule_ev_single_day(hh, start_time, end_time_update, da_time_prioritization)

        # ev_power_scheduled, charging_successful = _schedule_ev_power(hh, da_time_prioritization,
        #                                                            start_time, end_time_update)
        # if (not charging_successful) and with_bw:
        #     # print("setting in bw charging to False")
        #     # if charging inside bw couldn't be done, charge again with filling up spots outside of bw
        #     ev_power_scheduled, c_s = _schedule_ev_power(hh, da_time_prioritization, start_time, end_time_update,
        #                                                ev_power_previously_scheduled=ev_power_scheduled,
        #                                                in_bw_charging=False)
        # elif (not charging_successful) and (not with_bw):
        #     # this should be extremely rare and only occur for quite extreme charge constraints.
        #     print("Fulfilling charge requirement is not possible based on the maximal charge power.")
        # # to simulate forward looking behavior, ev power is first scheduled for minimum requirement with known
        # # prices, and then again for desired requirement over optimization time horizon with forecasted prices
        # if strategy == 'opt-ind':
        #     ev_power_scheduled, charging_successful = \
        #         _schedule_ev_power(hh, sorted_prices_forecast, start_time, end_time_opt,
        #                           ev_power_previously_scheduled=ev_power_scheduled)
        yield hh.ident, ev_power_scheduled
