import pandas as pd
from functools import reduce
from typing import Dict
from datetime import datetime, timedelta
from copy import deepcopy
from flexibleloadtariffs.models import central_optimization_model, individual_scheduling
from flexibleloadtariffs.simulation import config

# globals
time_step = int(config['time step'])
system_messages = config['system messages']
trafo_upgrade_factor = config['trafo upgrade factor']

class Neighborhood:
    def __init__(self, households: Dict, price_time_series: pd.Series, transformer_size: float,
                 transformer_loss_factor: float, transformer_upgrade_factor: float,
                 transformer_replacement_fraction: float,
                 charge_strategy: str, tariff_revenue_requirement: float,
                 fixed_costs: float, current_date: datetime):
        """

        :param households: dictionary of all households initialized with their power curves
        :param price_time_series: pandas time series of wholesale price, should have the same
        time index as household power curves
        :param transformer_size: rated transformer capacity in kW
        :param charge_strategy: strategy used for ev charging
        :param tariff_revenue_requirement: tariff revenue required by the network operator
        :param current_date: datetime object of current date, should be initialized with first datetime
        of household power curves
        :param bw_on: whether or not to simulate with bandwidth in case of a capacity subscription tariff
        """
        # fixed values throughout simulation
        self.households = households
        self.number_of_households = len(households)
        self.price_time_series = price_time_series
        self.transformer_size = transformer_size
        self.transformer_loss_factor = transformer_loss_factor
        self.transformer_upgrade_factor = transformer_upgrade_factor
        self.transformer_replacement_fraction = transformer_replacement_fraction
        self.transformer_upgrades = 0               # number of transformer upgrades triggered
        self.charge_strategy = charge_strategy
        self.tariff_revenue_requirement = tariff_revenue_requirement
        self.fixed_costs = fixed_costs
        self.hh_inflex_power_tss = (hh.inflex_power for hh in households.values())
        self.total_inflex_power_ts = reduce(lambda x, y: x.add(y), self.hh_inflex_power_tss)
        # initialize date, ev power and total power, will be updated after every update step
        self.current_date = current_date
        self.total_ev_power_ts = pd.Series(0, index=pd.to_datetime(self.total_inflex_power_ts.index))
        self.total_power_ts = deepcopy(self.total_inflex_power_ts)
    

    def update_simulation(self, update_time_interval, optimization_time_horizon):
        """
        Moves the simulation of the neighborhood forward by one update time interval.
        Computes ev charging schedules for all ev's and updates their power curves as well as the current date.
        There are two different end times, one for the optimization problem solver and one for when
        the simulation should next be updated. This ensures some forward looking-behavior when
        planning for the ev charging, as the scheduling is planned taking into account the longer
        optimization horizon, but is updated at the shorter update horizon.
        :param update_time_interval:
        :param optimization_time_horizon:
        :return: None
        """
        start_time = self.current_date
        # end times for optimization and charge updates. One time step has to be removed at the end to avoid double
        # scheduling
        end_time_opt = self.current_date + optimization_time_horizon - timedelta(hours=0, minutes=time_step)
        end_time_update = self.current_date + update_time_interval - timedelta(hours=0, minutes=time_step)
        # if system_messages:
        #     print("Solving optimization problem from {} to {}. Writing outputs until {}.".format(
        #         str(start_time), str(end_time_opt), str(end_time_update)))
        charge_schedules = {
            'opt-cen':
                lambda: central_optimization_model(self.households, self.price_time_series, start_time,
                                                   end_time_opt, self.transformer_size),
            'opt-ind':
                lambda: individual_scheduling(self.households, start_time, end_time_update, strategy='opt-ind',
                                              da_price_ts=self.price_time_series, end_time_opt=end_time_opt),
            'charge-arr':
                lambda: individual_scheduling(self.households, start_time, end_time_update, strategy='charge-arr',
                                              da_price_ts=self.price_time_series,),
        }
        # update households
        for hh_ident, ev_powercurve in charge_schedules[self.charge_strategy]():
            self.households[hh_ident].update_ev_power(ev_powercurve, start_time, end_time_update)
            self.total_ev_power_ts.loc[start_time: end_time_update] += ev_powercurve.loc[start_time: end_time_update]
        # update neighborhood
        # ev_powers = (hh.ev_power for hh in self.households.values())
        # self.total_ev_power_ts = reduce(lambda x, y: x.add(y), ev_powers)
        self.total_power_ts.loc[start_time: end_time_update] += self.total_ev_power_ts.loc[start_time: end_time_update]
        if self.total_power_ts.max() > self.transformer_replacement_fraction * self.transformer_size:
            self.transformer_upgrade(self.transformer_upgrade_factor)
            print("transformer upgrade triggered. Total transformer upgrades: " + str(self.transformer_upgrades))
        self.current_date = end_time_update + timedelta(hours=0, minutes=time_step)

    def transformer_upgrade(self, upgrade_factor):
        # once a transformer upgrade is triggered, we use the new transformer size for all loss computations
        # for the whole time series
        self.transformer_size *= upgrade_factor
        self.transformer_upgrades += 1


class Transformer:
    def __init__(self, rated_capacity, loss_factor, age, lifetime, asset_cost, installation_cost, upgrade_factor):
        self.size = rated_capacity
        self.loss_factor = loss_factor
        self.age = age
        self.lifetime = lifetime
        self.asset_cost = asset_cost
        self.installation_cost = installation_cost
        self.upgrade_factor = upgrade_factor

    def upgrade(self, upgrade_factor):
        # once a transformer upgrade is triggered, we use the new transformer size for all loss computations
        # for the whole time series
        self.size *= upgrade_factor
        self.age = 0


