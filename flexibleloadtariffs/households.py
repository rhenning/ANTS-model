import pandas as pd
from datetime import datetime
from pathlib import Path
from os import listdir
from flexibleloadtariffs.utils.load_inputs import load_single_time_series_from_csv
from flexibleloadtariffs.utils.helpers import power_to_load_value
from flexibleloadtariffs.simulation import config

# globals
time_step = config['time step']
conn_limit = config['household conn limit']
capacity_value = config['capacity value']

reporoot_dir = Path(__file__).resolve().parent.parent


class Household:
    def __init__(self, ident, inflex_power: pd.Series, aggregator=None, ev=None, tariff=None, name=None, maxpower=None):
        self.ident = ident

        # create empty time series for EV power
        self.ev_power = pd.Series(0, index=pd.to_datetime(inflex_power.index))
        self.total_power = inflex_power
        self.inflex_power = self.total_power - self.ev_power
        self.aggregator = aggregator
        self.ev = ev
        self.tariff = tariff
        self.name = name
        self.maxpower = maxpower
        self.capacity_limit_by_month = None


    # def inflex_power(self):
    #     return self.total_power - self.ev_power

    def compute_capacity_limit_by_month(self):
        monthly_capacity_limit = {}
        for month in range(1, 13):
            if self.tariff.capacity_rule is not None:
                monthly_capacity_limit[month] = self.tariff.capacity_rule.capacity_bw
            elif self.tariff.personal_peak_rule is not None:
                powercurve_this_month = self.inflex_power[self.inflex_power.index.month == month]
                if not powercurve_this_month.empty:
                    average_peak = powercurve_this_month.nlargest(
                        self.tariff.personal_peak_rule.n_peaks_month).mean()
                    considered_peak = max(self.tariff.personal_peak_rule.min_peak, average_peak)
                    monthly_capacity_limit[month] = considered_peak
            else:
                monthly_capacity_limit[month] = conn_limit
        return monthly_capacity_limit

    def update_ev_power(self, ev_powercurve: pd.Series, start_time, end_time):
        """
        TODO start and end time could be inferred from given ev power. This method might be better in the EV class.
        Writes given ev power to the household object and updates its battery charge.
        :param ev_powercurve: solution from optimization or charge on arrival scheduling.
        :param start_time:
        :param end_time:
        :return: None
        """
        ev_power = ev_powercurve.loc[start_time: end_time]
        self.ev_power.loc[start_time: end_time] += ev_power
        self.total_power.loc[start_time: end_time] += self.ev_power.loc[start_time: end_time]
        self.ev.current_charge += power_to_load_value(ev_powercurve.sum())*self.ev.charging_efficiency
        # reduce charge by daily demand every time the inflex_power passes this EVs arrival time
        # first element of ev inflex_power is discarded to avoid double counting, as it is the same as
        # the last time step of the previous update
        for _ in ev_powercurve.iloc[1:].at_time(self.ev.arrival_time).iteritems():
            self.ev.current_charge -= self.ev.daily_demand
        self.ev.current_soc_perc = self.ev.current_charge/self.ev.battery_size
        if not 0 <= self.ev.current_soc_perc <= 1.000001:
            raise ValueError("ev state of charge out of bounds (0, 1) for household {}, ev {}: {}"
                             .format(self.ident, self.ev.ev_id, self.ev.current_soc_perc))


class Aggregator:
    def __init__(self, ident):
        self.ident = ident


defaultAggregator = Aggregator(1)


def _total_load_volume(powercurve):
    return sum([float(power) * time_step/ 60 for power in powercurve.values])


def compute_best_bw_tariff(powercurve, tariffs):
    """Computes the best bandwidth tariff choice for a household.

    Should be called with a tariff dictionary containing AT LEAST ONE bandwidth tariff.

    :param powercurve: household power curve
    :param tariffs: dictionary of available tariffs
    :return: the best tariff found
    """
    total_load_vol = _total_load_volume(powercurve)
    lowest_charge = float('inf')
    lowest_charge_bw = 0

    # The tariff dictionary should contain AT LEAST ONE bandwidth tariff, otherwise it will return None
    best_capacity_tariff = None

    for name, tariff in tariffs.items():
        if tariff.capacity_rule is not None:
            total_charge = tariff.total_charge(powercurve)
            tariff_bw = tariff.capacity_rule.capacity_bw
            avg_charge = tariff.avg_charge_per_kWh(powercurve)
            # print("Total charge for " + str(name) + ": " + str(total_charge))
            # print("Charge per kWh for " + str(name) + ": " + str(avg_charge))
            if total_charge - tariff_bw*capacity_value < lowest_charge - lowest_charge_bw*capacity_value:
                lowest_charge = total_charge
                lowest_charge_bw = tariff_bw
                best_capacity_tariff = tariff
    # print("assigning {} based on total charge and capacity size".format(best_capacity_tariff.name))
    return best_capacity_tariff


# noinspection PyPep8Naming
def compute_charge_per_kWh(powercurve, tariff):
    charge = tariff.total_charge(powercurve)
    total_kwh = _total_load_volume(powercurve)
    return charge / total_kwh


def load_households(inputs):
    """Loads household power data and creates household objects with them.

    Automatically computes best bw Tariff.

    :param inputs: list of input specifications
    :param tariffs: dictionary of available tariffs
    :return: dictionary of households indexed by household id
    """
    load_dir = reporoot_dir / Path(inputs['load data folder'])
    file_list = listdir(load_dir)
    households = {}
    hh_id = 0
    file_name = ''
    hh_load_df = None
    time_col = inputs['load csv time']
    date_col = inputs['load csv date']
    value_col = inputs['load csv values']
    load_datetime_format = inputs['load datetime format']
    config_datetime_format = inputs['datetime format']
    start_datetime = datetime.strptime(inputs['start datetime'], config_datetime_format)
    end_datetime = datetime.strptime(inputs['end datetime'], config_datetime_format)
    one_file_per_household = inputs['one file per household']
    time_step = int(inputs['time step'])
    decimal_separator = inputs['decimal separator']
    if not one_file_per_household:
        file_name = inputs['load data file']
        filepath = load_dir / file_name
        hh_load_df = pd.read_csv(reporoot_dir / filepath, sep=';', header=0)
        # combine data and time colums, convert to pandas datetime and set as index for time series
        hh_load_df = hh_load_df.set_index(pd.to_datetime(hh_load_df[date_col] + ' ' + hh_load_df[time_col],
                                                         format=load_datetime_format))
        # drop old date and time cols
        hh_load_df = hh_load_df.drop(columns=[date_col, time_col])
        # print(hh_load_df.head())
    # loop over households, reading in each file for LPG format and households as columns for Stedin
    for hh_nr in range(inputs['n_hh']):
        if one_file_per_household:
            file_name = file_list[hh_nr]
            print("Reading File: " + file_name)
            filepath = load_dir / file_name
            hh_load_ts = load_single_time_series_from_csv(filepath, time_col, value_col, load_datetime_format,
                                                          decimal_separator=decimal_separator)
        else:
            print("Reading household " + str(hh_nr) + " from file " + file_name)
            hh_load_ts = pd.Series(hh_load_df.iloc[:, hh_nr])
        data_freq = pd.infer_freq(hh_load_ts.index)
        if not str(data_freq) == '{}T'.format(time_step):
            print(data_freq)
            # resample to minutes given by timestep.
            hh_load_ts = hh_load_ts.resample('{}T'.format(time_step)).sum()
        # make time selection
        hh_load_ts = hh_load_ts.loc[start_datetime: end_datetime]
        # Multiply by 1h/timestep to convert from electric load to power over the timestep
        hh_power = hh_load_ts.multiply(int(60 / time_step))
        if one_file_per_household:
            hh_name = file_name[:-4].replace('_', ' ')
        else:
            hh_name = 'House ' + str(hh_nr)
        households[hh_id] = Household(hh_id, hh_power, aggregator=defaultAggregator,
                                      ev=None, tariff=None, name=hh_name, maxpower=hh_power.max())
        hh_id += 1
    return households
