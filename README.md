This project is intended to simulate different kinds of tariffs in realistic residential neighbourhoods and the implications of these tariffs for the scheduling of flexible loads. In particular, we want to benchmark the performance of different tariffs with respect to network regulatory objectives.

To run follow these steps:

1. Install Python on you computer: https://www.python.org/downloads/
2. Install pipenv: In a command line, type "pip install pipenv"
3. Clone this git repository onto your machine
4. Use a command line interface to navigate to the just-cloned folder and type "pipenv update"
5. Open a new pipenv shell by typing "pipenv shell"
    (note: when using pycharm on windows, the up/down arrows may not work inside the pipenv shell to get previously used command. In this case you can use the built-in virtual env function of pycharm: https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html#python_create_virtual_env)
6. To reproduce the results from the tariff assessment paper in Applied Energy, type "python main.py -e" with the preconfigured config file, or change the options in lpgconfig.yaml



