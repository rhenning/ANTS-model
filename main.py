from argparse import ArgumentParser
from flexibleloadtariffs.simulation import simulate


def handle_cmdline():
    parser = ArgumentParser()

    # required means that at one and only one of the possibilities must be chosen
    parser.add_argument(
        'strategy',
        choices=['opt-cen', 'opt-ind', 'charge-arr', 'no-ev'],
        help='Select a strategy (required): Either central optimization, individual optimization, charge-on-arrival'
             'or no ev scheduling (only inflexible household loads)'
    )

    parser.add_argument(
        '-e', '--varevs',  # chose '-e' for short form because '-v' is conventionally used for '--verbose'
        action='store_true',
        help='Optional flag to enable variable EV runs. Depending on the input settings, these can take a long time')


    parser.add_argument(
        '-c', '--config',
        default='03-configuration-examples/lpgconfig.yaml',
        type=str,
        help='Configuration file defining all formats, input parameters and tariffs used.')

    return parser.parse_args()


def main():
    parsed_args = handle_cmdline()
    simulate(parsed_args)


if __name__ == '__main__':
    main()
