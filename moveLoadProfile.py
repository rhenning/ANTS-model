import os
import shutil
from pathlib import Path

from flexibleloadtariffs.utils.load_inputs import load_single_time_series_from_csv

# time step used for resampling the load time series from 1 minute to time_step minutes
time_step = 15

reporoot_dir = Path(__file__).resolve().parent

data_dir = reporoot_dir / '01-datasets' / 'household-load' / '2020_LPG'

generate_subfolders_with_paths = (f.path for f in os.scandir(data_dir) if f.is_dir())

for i, household in enumerate(generate_subfolders_with_paths):
    household_name = 'household_{}.csv'.format(str(i+1))
    data_path = Path(household) / 'Results' / 'Overall.SumProfiles.Electricity.csv'
    target_path = data_dir / 'data' / household_name
    try:
        hh_load_ts = load_single_time_series_from_csv(data_path, 'Time', 'Sum [kWh]', '%d-%m-%Y %H:%M')
        hh_load_resampled = hh_load_ts.resample('{}T'.format(time_step)).sum()
        with open(target_path, newline='', mode='w+') as file:
            hh_load_resampled.to_csv(file, sep=';')
        # shutil.copy(data_path, target_path)
    except FileNotFoundError:
        continue

    # print(house_name)

# print(list_subfolders_with_paths)
